import json
import os
from typing import Callable, Dict, Optional, Any, Tuple, List

from flask import Flask, render_template, request, redirect

from GIST.DesignPopulation import DesignIteration, DesignPopulation
from GIST.HeuristicMap import HeuristicMap
from GIST.Registry.CallModifier import CallModifier
from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry
from GIST.Synthesizers import synthesizeFromSamples
from GIST.TacticObjectiveWeighting.TacticObjectiveGraph import getLearnedTactics
from GIST_App.AppConfiguration import AppConfiguration


class GISTFlaskApp:
    def __init__(self, app_name: str, app: Flask, registry: ObjectiveTacticRegistry, getTacticFunction: Callable, synthesizer: Callable, buildOptimizerFunction: Callable,
                 display_html='display.html', appConfig=AppConfiguration([])):
        self.appConfig = appConfig
        self.parameterResults = {}
        self.synthesizer: Callable = synthesizer
        self.display_html = display_html
        self.isFirstLoad: bool = True
        self.json_results: str = ""
        self.longPrint: Dict[str, Any] = {}
        self.shortPrint: str = ""
        self.objectiveFormData: Dict[str, Any] = {}
        self.heuristic_map: Optional[HeuristicMap] = None
        self.optimizerResults: Optional[Tuple[float, List[DesignIteration], DesignPopulation]] = None
        self.tactic_src: str = "learn"
        self.gui_tactic_to_obj_weights: Dict[CallModifier, Dict[str, int]] = {}
        self.app_name: str = app_name
        self._buildOptimizerFunction: Callable = buildOptimizerFunction
        self._getTacticFunction: Callable = getTacticFunction
        self.app: Flask = app
        self.registry: ObjectiveTacticRegistry = registry
        self.objectives: Dict[str, Dict[str, Any]] = {}
        for objectiveName, objectiveParameterDefaults in self.registry.objectiveNamesToParameters.items():
            parameters = {parameterName: objectiveParameterDefaults[parameterName] for parameterName in objectiveParameterDefaults}
            self.objectives[objectiveName] = parameters
        self.objectiveData: Dict[str, Dict[str, Dict[str, Any]]] = {}
        for objectiveName, parameters in self.objectives.items():
            paramDef = {}
            for parameterName, parameterValue in parameters.items():
                if type(parameterValue) is int:
                    typeStr = "int"
                    value = parameterValue
                elif type(parameterValue) is float:
                    typeStr = "float"
                    value = parameterValue
                elif type(parameterValue) is bool:
                    typeStr = "bool"
                    value = str(parameterValue)
                else:
                    assert type(parameterValue) is str
                    typeStr = "str"
                    value = parameterValue
                paramDef[parameterName] = {"type": typeStr, "value": value}
            self.objectiveData[objectiveName] = paramDef
        self.tactics: Dict[str, Dict[str, Any]] = {}
        for tacticName, tacticParameterDefaults in self.registry.tacticNamesToParameters.items():
            parameters = {parameterName: tacticParameterDefaults[parameterName] for parameterName in tacticParameterDefaults}
            self.tactics[tacticName] = parameters
        self.tacticData: Dict[str, Dict[str, Dict[str, Any]]] = {}
        for tacticName, parameters in self.tactics.items():
            paramDef = {}
            for parameterName, parameterValue in parameters.items():
                if type(parameterValue) is int:
                    typeStr = "int"
                    value = parameterValue
                elif type(parameterValue) is float:
                    typeStr = "float"
                    value = parameterValue
                elif type(parameterValue) is bool:
                    typeStr = "bool"
                    value = str(parameterValue)
                else:
                    assert type(parameterValue) is str
                    typeStr = "str"
                    value = parameterValue
                paramDef[parameterName] = {"type": typeStr, "value": value}
            self.tacticData[tacticName] = paramDef

    def getTactics(self) -> Dict[CallModifier, Dict[str, float]]:
        if self.tactic_src == "learn":
            return getLearnedTactics(modifier="_gui", tacticFile="cookie_tacticWeights.json", registry=self.registry, synthesizer=lambda: self.synthesizer(**self.parameterResults))
        elif self.tactic_src == "default":
            return self._getTacticFunction(modifier="_gui")
        else:  # from GUI
            return self.gui_tactic_to_obj_weights

    def buildOptimizer(self, heuristicMap: HeuristicMap, startWithOldResult: bool = False):
        optimizer = self._buildOptimizerFunction(heuristicMap, **self.parameterResults)
        if startWithOldResult:
            priorIterations = self.optimizerResults[1]
            optimizer.synthesizer = lambda: synthesizeFromSamples([iteration.design for iteration in priorIterations])
        return optimizer

    def main(self):
        """Entry point; the view for the main page"""
        obj_dat_str = str(self.objectiveData)
        obj_dat_str = obj_dat_str.replace("'", "\"")
        tac_dat_str = str(self.tacticData)
        tac_dat_str = tac_dat_str.replace("'", "\"")
        template = render_template('index.html', name=self.app_name, objectives=self.objectives, objective_data=obj_dat_str, tactics=self.tactics, tactic_data=tac_dat_str,
                                   config_params=self.appConfig.toHtml_str())
        return template

    def loadResult(self):
        if request.method == "POST":
            if "update_input" in request.form:
                return self.buildAndRunOptimizer(True)
            else:
                longPrintStr = request.form["result_longPrint"]
                self.longPrint = json.loads(longPrintStr)
                self.shortPrint = request.form["result_shortPrint"]
                return redirect('/display')
        else:
            return "Error: No result posted"

    def displayResult(self):
        return render_template(self.display_html, **self.longPrint)

    def getObjectiveData(self):
        if request.method == "POST":
            # get config values
            self.parameterResults = {}
            for name in request.form:
                if "config_" in name:
                    self.parameterResults[name[len("config_"):]] = request.form[name]
            # get GIST values
            self.tactic_src = request.form['select_tactic_src']
            objWeightMap_text = request.form['objWeightMap_text']
            objectiveWeights = {}
            self.objectiveFormData = json.loads(objWeightMap_text)
            for modifiedObjectiveName, objData in self.objectiveFormData.items():
                objWeight = objData['objWeight']
                paramValues = objData['paramValues']
                objectiveName = objData['objName']
                modifier = "_gui"
                if modifiedObjectiveName != objectiveName:
                    diff = modifiedObjectiveName[modifiedObjectiveName.index(objectiveName) + len(objectiveName):]
                    modifier = diff + modifier
                callModifier = CallModifier(self.registry, objectiveName, modifier, **paramValues)
                objectiveWeights[callModifier] = objWeight
            tacticWeight_text = request.form['tacticWeight_text']
            tacticFormData = json.loads(tacticWeight_text)
            for modifiedTacticName, tacticData in tacticFormData.items():
                tacticName = tacticData["tacticName"]
                paramValues = tacticData['paramValues']
                tToOWeights = tacticData['objectives']
                modifier = "_gui"
                if modifiedTacticName != tacticName:
                    diff = modifiedTacticName[modifiedTacticName.index(tacticName) + len(tacticName):]
                    modifier = diff + modifier
                callModifier = CallModifier(self.registry, tacticName, modifier, **paramValues)
                self.gui_tactic_to_obj_weights[callModifier] = tToOWeights
            tactics = self.getTactics()
            self.heuristic_map = HeuristicMap(objectivesToWeights=objectiveWeights, tacticsToObjectiveWeights=tactics, registry=self.registry)
            return self.buildAndRunOptimizer()
        else:
            return "Error: Results not Posted"

    def buildAndRunOptimizer(self, startWithOldResult: bool = False):
        optimizer = self.buildOptimizer(self.heuristic_map, startWithOldResult)
        self.optimizerResults = optimizer.runOptimizer()
        population = self.optimizerResults[2]
        loadableResults = {}  # total_score -> {shortPrint, longPrint, obj_name -> [obj_weight, base_score, shortPrint, longPrint]}
        for score, iterations in reversed(population.designsByScore.items()):
            iteration = iterations[0]
            try:
                shortPrint = iteration.design.shortPrint()
            except AttributeError:
                shortPrint = None
            try:
                longPrint: Optional[Dict[str, Any]] = iteration.design.longPrint()
            except AttributeError:
                longPrint = None
            baseScores = self.heuristic_map.collectScores(iteration.design)
            obj_data = {}
            for objectiveName, baseScore in baseScores.items():
                localName = objectiveName
                if "_gui" in objectiveName:
                    localName = objectiveName[:-1 * len("_gui")]
                obj_data[localName] = {"obj_weight": self.heuristic_map.objectiveNamesToWeights[objectiveName], "base_score": baseScore}
            loadableResults[score] = {"short_print": shortPrint, "long_print": longPrint, "objectives": obj_data}
        self.json_results = json.dumps(loadableResults)
        return redirect("/results")

    def run(self, host: str = "127.0.0.5"):
        self.app.templates_auto_reload = True
        self.app.run(debug=True, host=host)

    def results(self):
        if self.isFirstLoad:
            load_or_update = "Load"
        else:
            load_or_update = "Update"
        return render_template("results.html", name=self.app_name, results=self.json_results, load_or_update=load_or_update)


def makeApp():
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    GIST_DIR = ROOT_DIR[:ROOT_DIR.index("GIST")]
    return Flask(__name__, root_path=GIST_DIR)
