from enum import Enum
from typing import Dict, Optional, List

from html_writer import Html


class ParamType(Enum):
    Number = "number"
    String = "text"
    Boolean = "checkbox"
    choice = "choice"

    def htmlType(self) -> str:
        return self.value


class AppParameter:
    def __init__(self, paramId: str, label: str, paramType: ParamType, defaultValue, choices: Optional[List[str]] = None, additionalAttributes: Optional[Dict[str, str]] = None):
        self.form: str = "settings"
        self.choices: Optional[List[str]] = choices
        self.paramType: ParamType = paramType
        self.defaultValue = defaultValue
        self.paramId: str = paramId
        self.label: str = label
        if additionalAttributes is None:
            self.additionalAttributes = {}
        else:
            self.additionalAttributes: Dict[str, str] = additionalAttributes

    def toHTML(self, html: Optional[Html] = None) -> Html:
        if html is None:
            html = Html()
        with html.tag('label', attributes={"for": self.paramId}) as label:
            label += self.label

        inputAttributes = {"id": self.paramId, "form": self.form}
        inputAttributes.update(self.additionalAttributes)
        if self.paramType is ParamType.choice:
            with html.tag('select', attributes=inputAttributes):
                for choice in self.choices:
                    with html.tag('option', attributes={"value": choice}) as option:
                        option += choice
        else:
            inputAttributes.update({"id": self.paramId, "name": f"config_{self.paramId}", "type": self.paramType.htmlType(), "value": self.valueStr()})
            if self.paramType is ParamType.Boolean and self.defaultValue:
                inputAttributes["checked"] = self.valueStr()
            html.self_close_tag('input', attributes=inputAttributes)
        return html

    def toHtml_str(self) -> str:
        return str(self.toHTML())

    def valueStr(self) -> str:
        if type(self.defaultValue) is bool:
            if self.defaultValue:
                return 'true'
            else:
                return 'false'
        else:
            return str(self.defaultValue)

    def __str__(self):
        return self.paramId

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))


class AppConfiguration:
    def __init__(self, parameters: List[AppParameter]):
        self.parameters: List[AppParameter] = parameters

    def toHtml(self):
        html = Html()
        html.self_close_tag("br")
        for parameter in self.parameters:
            html = parameter.toHTML(html)
            html.self_close_tag("br")
        return html

    def toHtml_str(self):
        return str(self.toHtml())
