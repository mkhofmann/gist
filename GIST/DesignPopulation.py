import random

from sortedcontainers import SortedDict
from typing import List, Optional, Tuple

from GIST.HeuristicMap import HeuristicMap


class DesignPopulation:
    def __init__(self, heuristicMap: HeuristicMap, populationCap: int = 10):
        self.heuristicMap = heuristicMap
        self.populationCap: int = populationCap
        self.designCount: int = 0
        self.designsByScore: SortedDict = SortedDict()
        self.scoreByDesign: SortedDict = SortedDict()
        self.scores: List[float] = []  # the scores in the order they come in
        self.scoreSteps: List[float] = []  # the steps taken between scores
        self._scoreSum: float = 0.0
        self.popCount = 0

    def addDesign(self, design, score):
        # add to population
        self._addScore(score)
        designIteration = DesignIteration(design, self)
        if score not in self.designsByScore:
            self.designsByScore[score] = []
        self.designsByScore[score].append(designIteration)
        self.scoreByDesign[designIteration] = score
        self.popCount += 1

        if self.popCount > self.populationCap:  # remove worst from population
            min_score = self._getMinScore()
            worst_design = self.designsByScore[min_score].pop()  # remove the latest worst-design
            if len(self.designsByScore[min_score]) == 0:
                del self.designsByScore[min_score]
            del self.scoreByDesign[worst_design]
            self.popCount -= 1

        assert self.popCount == len(self.scoreByDesign)
        assert self.popCount <= self.populationCap
        return designIteration

    def getMaxScoringDesigns(self):
        score, iterations = self.designsByScore.peekitem()
        return score, iterations, self

    def _getMinScore(self):
        return self.designsByScore.peekitem(0)[0]

    def getMaxScore(self) -> float:
        if len(self.designsByScore) == 0:
            return 0.0
        return self.designsByScore.peekitem()[0]

    def getRandomDesignByScore(self, score):
        assert score in self.designsByScore
        return random.choice(self.designsByScore[score])

    def _lastScore(self) -> Optional[float]:
        if len(self.scores) > 0:
            return self.scores[-1]
        else:
            return None

    def _addScore(self, newScore: float):
        maxScore = self.getMaxScore()
        step = newScore - maxScore
        self.scoreSteps.append(step)
        self.scores.append(newScore)
        self._scoreSum += newScore

    def meanScore(self):
        return self._scoreSum / float(len(self.scores))

    def getStep(self, designIteration) -> float:
        return self.scoreSteps[designIteration.iteration]

    def __len__(self):
        return self.designCount

    def standardDeviation(self) -> float:
        mean = self.meanScore()
        deviation_sum = sum([(score - mean) ** 2 for score in self.scoreByDesign.values()])
        std = deviation_sum / float(len(self.scoreByDesign))
        return std


class DesignIteration:
    def __init__(self, design, population: DesignPopulation):
        self.design = design
        self.iteration = population.designCount
        population.designCount += 1

    def __hash__(self):
        return self.iteration

    def __lt__(self, other):
        return self.iteration < other.iteration

    def __gt__(self, other):
        return self.iteration > other.iteration

    def __eq__(self, other):
        return self.iteration == other.iteration

    def __str__(self):
        return f"i_{self.iteration} == {self.design}"
