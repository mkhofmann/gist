import random


def randomNumber() -> int:
    return random.randint(1, 100)
