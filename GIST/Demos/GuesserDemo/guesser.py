import random
from typing import Dict

from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry
from GIST.Registry.TacticRegistration import incrementProperty_tactic
from GIST.Demos.GuesserDemo.random_number import randomNumber
from GIST.Selectors.DesignSelectors import biasedRandomDesignSelection
from GIST.GISTOptimizer import GISTOptimizer
from GIST.HeuristicMap import HeuristicMap
from GIST.Registry.ObjectiveRegistration import approachingOrLessThanValue_objective, approachingOrGreaterThanValue_objective
from GIST.Registry.CallModifier import CallModifier
from GIST.StoppingCriteria import thresholdScoreReached
from GIST.Selectors.TacticSelectors import biasedByExpectedImprovementScore


class Guesser:
    Guesser_Registry = ObjectiveTacticRegistry()

    def __init__(self, guess=None):
        if guess is not None:
            self._guess = guess
        else:
            self._guess: float = randomNumber()

    @approachingOrLessThanValue_objective(objectiveName="greaterThanGuess", registry=Guesser_Registry)  # value should be near or less than ACTUAl
    @approachingOrGreaterThanValue_objective(objectiveName="lessThanGuess", registry=Guesser_Registry)  # value should be near or greater than ACTUAL
    @property
    def guess(self):
        return self._guess

    @incrementProperty_tactic(increment=-1.0, tacticName="decrement_guess", registry=Guesser_Registry)  # if greater than guess then decrement
    @incrementProperty_tactic(increment=1.0, tacticName="increment_guess", registry=Guesser_Registry)  # if less than guess then increment
    @guess.setter
    def guess(self, value: float):
        self._guess = value

    def __str__(self) -> str:
        return str(self.guess)

    def shortPrint(self) -> str:
        return f"Guess == {self.guess}"

    def longPrint(self) -> str:
        return self.shortPrint()

    def filterGuessByRange(self) -> bool:
        return 1 <= self.guess <= 100

    def __hash__(self):
        return hash(self.guess)

    @staticmethod
    def getTactics(modifier) -> Dict[CallModifier, Dict[str, float]]:
        greater_name = f"greaterThanGuess{modifier}"
        lesser_name = f"lessThanGuess{modifier}"
        tactics = {CallModifier(Guesser.Guesser_Registry, "increment_guess"): {lesser_name: 1.0},
                   CallModifier(Guesser.Guesser_Registry, "decrement_guess"): {greater_name: 1.0}}
        return tactics

    @staticmethod
    def guesserObjectives(actualVal: int) -> Dict[CallModifier, float]:
        return {CallModifier(Guesser.Guesser_Registry, "greaterThanGuess", f"_{actual}", targetValue=actualVal, distance_bound=50.0): 1.0,
                CallModifier(Guesser.Guesser_Registry, "lessThanGuess", f"_{actual}", targetValue=actualVal, distance_bound=50.0): 1.0}

    @staticmethod
    def getOptimizer(heuristicMap: HeuristicMap) -> GISTOptimizer:
        return GISTOptimizer(synthesizer=Guesser,
                             filters=[Guesser.filterGuessByRange],
                             heuristicMap=heuristicMap,
                             designSelector=biasedRandomDesignSelection,
                             stoppingCriteria=lambda dI, dP: thresholdScoreReached(_=dI, designPopulation=dP, thresholdPercentMaxScore=2.0),
                             tacticSelector=biasedByExpectedImprovementScore,
                             startingDesigns=10)


if __name__ == "__main__":
    random.seed(1)
    for i in range(0, 10):
        actual = randomNumber()
        # Generate 10 samples between 0 and 100 then iterate until a perfect score of 2.0 is reached
        objectiveWeights = Guesser.guesserObjectives(actual)
        tacticsWeights = Guesser.getTactics()
        heuristic_map = HeuristicMap(objectivesToWeights=objectiveWeights, tacticsToObjectiveWeights=tacticsWeights, registry=Guesser.Guesser_Registry)
        optimizer = Guesser.getOptimizer(heuristic_map)
        results = optimizer.runOptimizer()
        finalGuess = results[1][0].design.guess
        print(f"After {results[2].designCount} guess, we guessed {actual} == {finalGuess}")
        assert finalGuess == actual
