import random

from GIST.Demos.GuesserDemo.guesser import Guesser
from GIST.Demos.GuesserDemo.random_number import randomNumber
from GIST_App.GistFlaskApp import GISTFlaskApp, makeApp

random.seed(1)
app = makeApp()
actual = randomNumber()
Guesser.Guesser_Registry.setObjectiveParameterDefault("greaterThanGuess", "targetValue", actual)
Guesser.Guesser_Registry.setObjectiveParameterDefault("lessThanGuess", "targetValue", actual)
Guesser.Guesser_Registry.setObjectiveParameterDefault("greaterThanGuess", "distance_bound", 50.0)
Guesser.Guesser_Registry.setObjectiveParameterDefault("lessThanGuess", "distance_bound", 50.0)
gistApp = GISTFlaskApp("Guesser", app, Guesser.Guesser_Registry, Guesser.getTactics, Cookie, Guesser.getOptimizer)


@app.route('/')
@app.route('/index')
def main():
    # return render_template('index.html')
    return gistApp.main()


@app.route('/results')
def results():
    return gistApp.results()


@app.route('/display')
def display():
    return gistApp.displayResult()


@app.route('/', methods=["GET", "POST"])
@app.route('/index', methods=["GET", "POST"])
def getObjectivesData():
    return gistApp.getObjectiveData()


@app.route("/results", methods=["GET", "POST"])
def getResult():
    return gistApp.loadResult()


if __name__ == "__main__":
    # app.templates_auto_reload = True
    # app.run(debug=True, host="127.0.0.5")
    gistApp.run()
