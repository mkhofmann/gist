from enum import Enum
from typing import Dict, Any

from GIST.GISTOptimizer import GISTOptimizer
from GIST.HeuristicMap import HeuristicMap
from GIST.Registry.CallModifier import CallModifier
from GIST.Registry.ObjectiveRegistration import objective
from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry
from GIST.Registry.TacticRegistration import method_tactic
from GIST.Selectors.DesignSelectors import biasedRandomDesignSelection
from GIST.Selectors.TacticSelectors import biasedByExpectedImprovementScore
from GIST.StoppingCriteria import thresholdScoreReached


class Fat(Enum):
    Butter = "butter"
    MeltedB = "melted butter"
    Shortening = "shortening"

    def isMelted(self) -> bool:
        return self.value == Fat.MeltedB.value

    def spreads(self) -> bool:
        return self.value != Fat.Shortening.value

    def makeSpread(self):
        if self.spreads():
            return self
        else:
            return Fat.Butter

    @staticmethod
    def makePuff():
        return Fat.Shortening

    @staticmethod
    def melt():
        return Fat.MeltedB

    def __str__(self):
        return self.value

    def __repr__(self):
        return self.value


class Flour(Enum):
    Bread = "Bread flour"
    AP = "All Purpose flour"
    Cake = "Cake flour"

    def gluten(self) -> int:
        if self.value == Flour.Bread.value:
            return 2
        elif self.value == Flour.AP.value:
            return 1
        else:
            return 0

    def increaseGluten(self):
        if self.value == Flour.Cake.value:
            return Flour.AP
        else:
            return Flour.Bread

    def decreaseGluten(self):
        if self.value == Flour.Bread.value:
            return Flour.AP
        else:
            return Flour.Cake

    def __str__(self):
        return self.value

    def __repr__(self):
        return self.value


class Leavening(Enum):
    Powder = "baking powder"
    Soda = "baking soda"

    def isAcidic(self) -> bool:
        if self.value == Leavening.Soda.value:
            return False
        else:
            return True

    def reduceAcidity(self):
        if self.value == Leavening.Soda.value:
            return self
        else:
            return Leavening.Powder

    def increaseAcidity(self):
        if self.value == Leavening.Soda.value:
            return Leavening.Powder
        else:
            return self

    def __str__(self):
        return self.value

    def __repr__(self):
        return self.value


class Cookie:
    Cookie_Registry = ObjectiveTacticRegistry()

    def __init__(self, cookie_count: int,
                 fatType: Fat = Fat.Butter,
                 flourType: Flour = Flour.AP,
                 leavening: Leavening = Leavening.Soda,
                 whiteSugar: float = 1, brownSugar: float = .5,
                 wholeEggs: int = 2, extraEggYokes: int = 0, milk: float = 0):
        self.cookie_count: int = cookie_count
        self.leavening: Leavening = leavening
        self.leaveningAmount: float = 1.25  # teaspoons
        self.fatType: Fat = fatType
        self.fatAmount: float = 1.0  # cup
        self.flourType: Flour = flourType
        self.flourAmount: float = 2.25  # grams
        self.salt: float = 1  # teaspoon
        self.whiteSugar: float = whiteSugar  # cup
        self.brownSugar: float = brownSugar  # cup
        self.wholeEggs: int = wholeEggs
        self.eggYokes: int = extraEggYokes
        self.milk: float = milk  # tablespoons
        self.vanilla: float = 1.5  # teaspoon
        self.chocolate: float = 2.0  # cups

    @method_tactic("ReduceEggWhites", Cookie_Registry)
    def tradeEggWhitesForMilk(self):
        if self.wholeEggs > 0:
            self.wholeEggs -= 1
            self.milk += 2
            self.eggYokes += 1

    @method_tactic("ReduceEggs", Cookie_Registry)
    def tradeEggForMilk(self):
        if self.wholeEggs > 0:
            self.wholeEggs -= 1
            self.milk += 4
        elif self.eggYokes > 0:  # replace yoke with milk
            self.eggYokes -= 1
            self.milk += 2

    @method_tactic("ReduceMilk", Cookie_Registry)
    def reduceMilk(self):
        if self.milk > 0:
            if self.eggYokes > 0:  # add egg white back to egg yoke to make whole egg
                self.milk -= 2
                self.eggYokes -= 1
                self.wholeEggs += 1
            else:  # reduce egg by egg white and replace with milk
                self.wholeEggs -= 1
                self.milk -= 2
                self.eggYokes += 1

    @method_tactic("MakeAcidic", Cookie_Registry)
    def increaseAcidity(self):
        self.leavening = self.leavening.increaseAcidity()

    @method_tactic("MakeBasic", Cookie_Registry)
    def decreaseAcidity(self):
        self.leavening = self.leavening.reduceAcidity()

    @method_tactic("IncreaseWhiteSugar", Cookie_Registry)
    def increaseWhiteToBrownSugar(self):
        if self.brownSugar > 0:
            self.whiteSugar += .25
            self.brownSugar -= .25

    @method_tactic("IncreaseBrownSugar", Cookie_Registry)
    def decreaseWhiteToBrownSugar(self):
        if self.whiteSugar > 0:
            self.brownSugar += .25
            self.whiteSugar -= .25

    @method_tactic("SpreadableFat", Cookie_Registry)
    def makeFatSpread(self):
        self.fatType = self.fatType.makeSpread()

    @method_tactic("PuffyFat", Cookie_Registry)
    def makeFatPuff(self):
        self.fatType = Fat.Shortening

    @method_tactic("IncreaseFlourProtein", Cookie_Registry)
    def increaseFlourProtein(self):
        self.flourType = self.flourType.increaseGluten()

    @method_tactic("DecreaseFlourProtein", Cookie_Registry)
    def decreaseFlourProtein(self):
        self.flourType = self.flourType.decreaseGluten()

    @method_tactic("MeltButter", Cookie_Registry)
    def increaseFatWater(self):
        self.fatType = Fat.MeltedB

    @objective("Crisp", Cookie_Registry)
    def isCrisp(self):
        """
        soda instead of powder
        replace eggs with milk
        increase ratio of white to brown sugar
        use butter instead of shortening
        :return: 0->1 where 1 is the crispiest cookie
        """
        spreadContribution = self.estimateSpread() / 4.0
        return (self.whitePortion + spreadContribution) / 2.0

    @objective("Cakey", Cookie_Registry)
    def isCakey(self):
        """
        cake flour
        powder instead of soda
        shortening instead of butter
        :return: 0->1 where 1 is the cakiest cookie
        """
        glutenContribution = 1.0 - (self.estimateGluten() / 3.0)
        puffContribution = self.estimatePuff() / 4.0
        return (glutenContribution + puffContribution) / 2.0

    @objective("Chewy", Cookie_Registry)
    def isChewy(self) -> float:
        """
        melted butter
        bread flower
        reduce egg whites
        use more brown than white sugar
        :return: 0->1 where 1 is the chewiest cookie
        """
        glutenContribution = self.estimateGluten() / 3.0
        waterContribution = self.estimateWaterRetention() / 4.0
        return (glutenContribution + waterContribution) / 2.0

    @staticmethod
    def getTactics(modifier: str) -> Dict[CallModifier, Dict[str, float]]:
        crisp = f"Crisp{modifier}"
        cakey = f"Cakey{modifier}"
        chewy = f"Chewy{modifier}"
        tactics = {CallModifier(Cookie.Cookie_Registry, "ReduceEggWhites"): {crisp: .5, chewy: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "ReduceEggs"): {crisp: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "ReduceMilk"): {cakey: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "MakeAcidic"): {crisp: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "MakeBasic"): {cakey: 1.0, chewy: .5},
                   CallModifier(Cookie.Cookie_Registry, "IncreaseWhiteSugar"): {crisp: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "IncreaseBrownSugar"): {chewy: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "SpreadableFat"): {crisp: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "PuffyFat"): {chewy: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "IncreaseFlourProtein"): {chewy: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "DecreaseFlourProtein"): {cakey: 1.0},
                   CallModifier(Cookie.Cookie_Registry, "MeltButter"): {chewy: 1.0}}
        return tactics

    @staticmethod
    def getOptimizer(heuristicMap: HeuristicMap, **kwargs) -> GISTOptimizer:
        return GISTOptimizer(synthesizer=lambda: Cookie(**kwargs),
                             filters=[],
                             heuristicMap=heuristicMap,
                             designSelector=biasedRandomDesignSelection,
                             stoppingCriteria=lambda dI, dP: thresholdScoreReached(_=dI, designPopulation=dP, thresholdPercentMaxScore=.9),
                             tacticSelector=biasedByExpectedImprovementScore,
                             startingDesigns=1)

    def shortPrint(self):
        return f"Crisp =={self.isCrisp()}, Cakey == {self.isCakey()}, Chewy == {self.isChewy()}"

    def longPrint(self) -> Dict[str, Any]:
        crisp = self.isCrisp()
        cakey = self.isCakey()
        chewy = self.isChewy()
        if crisp > max(cakey, chewy):
            name = "Crispy Cookie"
        elif cakey > max(crisp, chewy):
            name = "Cakey Cookie"
        else:
            name = "Chewy Cookie"

        return {"Name": name, "cookie_count": self.cookie_count, "flourType": str(self.flourType), "leavening": str(self.leavening), "wholeEggs": self.wholeEggs,
                "eggYokes": self.eggYokes, "milk": self.milk,
                "fatType": str(self.fatType), "whiteAmount": self.whiteSugar, "brownAmount": self.brownSugar}

    def estimateGluten(self) -> int:
        """
        gluten comes from higher protein flours and creates a chewy texture in bake goods
        Melted fats will increase the water available during gluten formation in batter making
        :return: 0->3 (2 flour and melted butter effect)
        """
        return self.flourType.gluten() + self.fatType.isMelted()

    def estimateAcidity(self) -> float:
        """
        Reducing batter acidity will raise the temperature at which the cookies set.
        A higher setting temperature will give the cookie more time to spread, creating a flatter cookie
        i.e., ^acidity^ == puff, V acidity V == spread
        Acidity is primarily created by adding a leavening agent but can also come from the molasses in brown sugar
        :return: 0-> 2 (1 for leavening 1 for brown sugar portion
        """
        return float(self.leavening.isAcidic()) + self.brownPortion

    def estimateSpread(self) -> float:
        """

        Eggs tend to puff (not spread), so replacing eggs with milk will increase spread
        Fats with sharp melting points (i.e., butter) will convert to liquid state before the batter can set, causing spreading
        Fats with higher melting points (i.e., shortening) will not melt until after the cookie has set, allowing it to rise
        Lower protein flours will not produce as much gluten (water+protein) during mixing, leaving it available for steam production in cakey cookies
        :return: 0 -> 4 (fat spreading +  2 acidity_effect + milk portion)
        """
        return float(self.fatType.spreads()) + self.estimateAcidity() + self.milkPortion

    def estimatePuff(self) -> float:
        """
        Reducing batter acidity will raise the temperature at which the cookies set.
        A higher setting temperature will give the cookie more time to spread, creating a flatter cookie
        Eggs tend to puff (not spread), so replacing eggs with milk will increase spread
        Fats with sharp melting points (i.e., butter) will convert to liquid state before the batter can set, causing spreading
        Fats with higher melting points (i.e., shortening) will not melt until after the cookie has set, allowing it to rise
        Lower protein flours will not produce as much gluten (water+protein) during mixing, leaving it available for steam production in cakey cookies
        :return: 0 -> 4 (fat spreading + 2 acidity_effect + egg white portion
        """
        return float(not self.fatType.spreads()) + (2.0 - self.estimateAcidity()) + (1.0 - self.milkPortion)

    def estimateWaterRetention(self):
        """
        White sugar reduces water content while brown sugar is hydroscopic (increases water retention).
        Cookies with higher white to brown ratios will crisp
        Cookies with lower white to brown ratios will be chewier
        Higher protein flours may retain water in cookie after baking, increasing chewiness
        Egg whites dry out baked goods and can be replaced with milk to increase water retention
        :return: 0-> 3 (3* sugar  + egg)
        """
        eggWhites = (1.0 - self.eggWhitePortion)
        return 3 * self.brownPortion + eggWhites

    @property
    def allSugar(self) -> float:
        return self.whiteSugar + self.brownSugar

    @property
    def animalProteins(self) -> float:
        """
        egg yokes count as 1/2 egg and 4tbsp of milk counts as a whole egg
        :return:
        """
        totalEggYokes = float(self.eggYokes) + (.5 * float(self.wholeEggs))
        milkActingAsEggs = self.milk / 4.0
        return totalEggYokes + self.eggWhites + milkActingAsEggs

    @property
    def eggWhites(self):
        return float(self.wholeEggs) / 2.0

    @property
    def eggWhitePortion(self) -> float:
        if self.eggQuantity == 0:
            return 0.0
        return self.eggWhites / self.eggQuantity

    @property
    def eggQuantity(self):
        return float(self.wholeEggs + self.eggYokes)

    @property
    def milkPortion(self) -> float:
        return float(self.milk / 4.0) / self.animalProteins

    @property
    def brownPortion(self) -> float:
        return float(self.brownSugar) / float(self.allSugar)

    @property
    def whitePortion(self) -> float:
        return float(self.whiteSugar) / float(self.allSugar)


def cookie_synth(cookie_count: int) -> Cookie:
    return Cookie(cookie_count=cookie_count)
