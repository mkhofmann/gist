from GIST.Demos.CookieGist.Cookie import Cookie
from GIST_App.AppConfiguration import AppConfiguration, AppParameter, ParamType
from GIST_App.GistFlaskApp import makeApp, GISTFlaskApp

app = makeApp()
appConfig = AppConfiguration([AppParameter("cookie_count", "Number of Cookies", ParamType.Number, 24)])
gistApp = GISTFlaskApp(app_name="Cookie", app=app, registry=Cookie.Cookie_Registry, getTacticFunction=Cookie.getTactics, synthesizer=Cookie,
                       buildOptimizerFunction=Cookie.getOptimizer, display_html="recipe.html", appConfig=appConfig)


@app.route('/')
@app.route('/index')
def main():
    # return render_template('index.html')
    return gistApp.main()


@app.route('/results')
def results():
    return gistApp.results()


@app.route('/display')
def display():
    return gistApp.displayResult()


@app.route('/', methods=["GET", "POST"])
@app.route('/index', methods=["GET", "POST"])
def getObjectivesData():
    return gistApp.getObjectiveData()


@app.route("/results", methods=["GET", "POST"])
def getResult():
    return gistApp.loadResult()


if __name__ == "__main__":
    gistApp.run()
