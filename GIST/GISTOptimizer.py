from typing import Callable, List, Optional, Tuple

from GIST.DesignPopulation import DesignIteration, DesignPopulation
from GIST.HeuristicMap import HeuristicMap, minimizeHeuristicMap


class GISTOptimizer:

    def __init__(self, synthesizer: Callable, filters: List[Callable], heuristicMap: HeuristicMap,
                 designSelector: Callable, stoppingCriteria: Callable, tacticSelector: Callable,
                 populationCap: int = 10, startingDesigns: int = 10, iterationCap: int = 1000):
        self.iterationCap = iterationCap
        self.populationCap: int = populationCap
        self.startingDesigns: int = startingDesigns
        self.synthesizer: Callable = synthesizer
        self.filters: List[Callable] = filters
        self.heuristicMap: HeuristicMap = heuristicMap
        self.designSelector: Callable = designSelector
        self.stoppingCriteria: Callable = stoppingCriteria
        self.tacticSelector: Callable = tacticSelector

    def minimizeHeuristicMap(self, tacticsPerObjective: Optional[int] = None, max_tactic_usage: Optional[int] = None):
        self.heuristicMap, _ = minimizeHeuristicMap(self.heuristicMap, self.heuristicMap.registry, tacticsPerObjective, max_tactic_usage)

    def runOptimizer(self) -> Tuple[float, List[DesignIteration], DesignPopulation]:

        def filterAndScore(d) -> Tuple[bool, Optional[DesignIteration], float]:
            if self.filterDesign(d):
                design_score = self.scoreDesign(d)
                design_iteration = designPopulation.addDesign(d, design_score)
                return True, design_iteration, design_score
            return False, None, -1.0

        designPopulation = DesignPopulation(self.heuristicMap, self.populationCap)
        for i in range(0, self.startingDesigns):
            design = self.synthesizer()
            _, iteration, score = filterAndScore(design)
            if score == self.heuristicMap.maxPossibleScore:
                return designPopulation.getMaxScoringDesigns()
            elif self.stoppingCriteria(iteration, designPopulation):
                return designPopulation.getMaxScoringDesigns()

        iterations = 0
        stop = False
        while not stop and iterations < self.iterationCap:
            designIteration = self.designSelector(designPopulation)
            tactic = self.tacticSelector(designIteration, self.heuristicMap)
            newDesign = tactic(designIteration.design)
            notFiltered, newIteration, newScore = filterAndScore(newDesign)
            if notFiltered:
                if newScore == self.heuristicMap.maxPossibleScore:
                    return designPopulation.getMaxScoringDesigns()
                stop = self.stoppingCriteria(newIteration, designPopulation)
                if stop:
                    return designPopulation.getMaxScoringDesigns()
            iterations += 1

        return designPopulation.getMaxScoringDesigns()

    def filterDesign(self, design) -> bool:
        for designFilter in self.filters:
            if not designFilter(design):
                return False
        return True

    def scoreDesign(self, design) -> float:
        return self.heuristicMap.scoreDesign(design)
