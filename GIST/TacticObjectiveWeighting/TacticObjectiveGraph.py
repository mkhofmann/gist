import json
from typing import Dict, Set, Any, Callable, List, Tuple, Optional, Iterable

from networkx import DiGraph

from GIST.Demos.GuesserDemo.guesser import Guesser
from GIST.HeuristicMap import HeuristicMap, readTacticsToObjectiveWeightsFromJson
from GIST.Registry.CallModifier import CallModifier, CallModifierAcrossRange
from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry

guess = Guesser(3)


class TacticObjectiveGraph:
    class SampleNode:
        def __init__(self, sample: Any, parentGraph):
            self.sample = sample
            self.parentGraph = parentGraph
            self.scores = parentGraph.design_scores(self.sample)

        def __hash__(self):
            return hash(self.sample)  # by default repr will give memory address which should be uniquely hashable

        def __str__(self):
            return str(self.sample)

        def __repr__(self):
            return str(self)

    class EdgePerformance:
        def __init__(self, first_nodeID: int, second_nodeID: int, parentGraph, *tactic_names):
            self.tactics: List[str] = [*tactic_names]
            assert first_nodeID in parentGraph
            assert second_nodeID in parentGraph
            self.firstNode: TacticObjectiveGraph.SampleNode = parentGraph.getNode(first_nodeID)
            self.secondNode: TacticObjectiveGraph.SampleNode = parentGraph.getNode(second_nodeID)
            self.score_diffs: Dict[str, float] = {objective_name: self.secondNode.scores[objective_name] - self.firstNode.scores[objective_name] for objective_name in
                                                  self.firstNode.scores}
            for objective_name, score_dif in self.score_diffs.items():
                parentGraph.maxDiffs[objective_name] = max(parentGraph.maxDiffs[objective_name], score_dif)
                parentGraph.minDiffs[objective_name] = min(parentGraph.minDiffs[objective_name], score_dif)

        def addTactic(self, tactic_name: str):
            self.tactics.append(tactic_name)

    def __init__(self, registry: ObjectiveTacticRegistry, seedSamples: List[Any], objectives: List[CallModifierAcrossRange], tactics: Set[CallModifierAcrossRange],
                 filters: Optional[List[Callable]] = None):
        self.registry = registry
        if filters is None:
            self.filters = []
        else:
            self.filters: List[Callable] = filters
        self._tactics: Dict[str:List[Callable]] = {}
        for tacticCallerRange in tactics:
            tactic_name = tacticCallerRange.generalName
            callModifiers = tacticCallerRange.modifiersOverRange()
            tactic_funcs = []
            for tactic_caller in callModifiers:
                tactic_func = tactic_caller.specificCall(isObjective=False)
                tactic_funcs.append(tactic_func)
                # global_objectiveTacticRegistry.addTactic(tactic_name, tactic_func)
            self._tactics[tactic_name] = tactic_funcs

        self._objectives: Dict[str: List[Callable]] = {}
        for objective_rangeModifier in objectives:
            callerModifiers = objective_rangeModifier.modifiersOverRange()
            self._objectives[objective_rangeModifier.generalName] = []
            for objectiveCaller in callerModifiers:
                objective_func = objectiveCaller.specificCall(isObjective=True)
                self._objectives[objective_rangeModifier.generalName].append(objective_func)

        self.sampleSpace: DiGraph = DiGraph()
        self.seeds: List[TacticObjectiveGraph.SampleNode] = []
        for sample in seedSamples:
            seedNode, isNew = self.addNode(sample)
            if isNew:
                self.seeds.append(seedNode)
        self.edgesByTactic: Dict[str, List[Tuple[int, int]]] = {}
        self.maxDiffs: Dict[str, float] = {objective_name: 0.0 for objective_name in self._objectives}
        self.minDiffs: Dict[str, float] = {objective_name: 0.0 for objective_name in self._objectives}

    def objective(self, objective_name: str, design: Any) -> float:
        assert objective_name in self._objectives
        scores = [objective_func(design) for objective_func in self._objectives[objective_name]]
        score_sum = sum(scores)
        score = score_sum / float(len(self._objectives[objective_name]))
        return score

    def score_design(self, design: Any) -> float:
        return sum(self.objective(objective_name, design) for objective_name in self._objectives)

    def design_scores(self, design: Any) -> Dict[str, float]:
        return {objective_name: self.objective(objective_name, design) for objective_name in self._objectives}

    def passesFilter(self, design: Any) -> bool:
        for filter_func in self.filters:
            if not filter_func(design):
                return False
        return True

    def addNode(self, design: Any) -> Tuple[Optional[SampleNode], bool]:
        if self.passesFilter(design):
            node = TacticObjectiveGraph.SampleNode(sample=design, parentGraph=self)
            nodeId = hash(node)
            if nodeId not in self.sampleSpace.nodes:
                self.sampleSpace.add_node(nodeId, node=node)
                return node, True
            return node, False
        else:
            return None, False

    def getNode(self, nodeID) -> Optional[SampleNode]:
        if nodeID in self.sampleSpace.nodes:
            return self.sampleSpace.nodes[nodeID]["node"]
        else:
            return None

    def getEdge(self, first_nodeId, second_nodeId) -> Optional[EdgePerformance]:
        if self.sampleSpace.has_edge(first_nodeId, second_nodeId):
            return self.sampleSpace[first_nodeId][second_nodeId]["edge"]
        else:
            return None

    def branch(self, curNode: SampleNode, tactic_name: str, tactic_func: Callable) -> Tuple[SampleNode, bool]:
        """
        Creates a new branch from the given node by applying the tactic to its sample
        Will create a new edge with the attribute "tactics" with a list of tactics that bridge the current node and the resulting node
        :param tactic_func:
        :param curNode: The current node to branch from
        :param tactic_name: the name of the tactic to apply
        :return: the node created by applying the tactic to the current node's sample design
        """

        newDesign = tactic_func(curNode.sample)
        newNode, isNew = self.addNode(newDesign)
        if newNode is not None:
            curNodeID = hash(curNode)
            newNodeId = hash(newNode)
            edge = self.getEdge(curNodeID, newNodeId)
            if edge is None:
                edge = TacticObjectiveGraph.EdgePerformance(curNodeID, newNodeId, self, tactic_name)
                self.sampleSpace.add_edge(curNodeID, newNodeId, edge=edge)
            else:  # an edge between these nodes already existed
                edge.addTactic(tactic_name)
            if tactic_name not in self.edgesByTactic:
                self.edgesByTactic[tactic_name] = []
            self.edgesByTactic[tactic_name].append((curNodeID, newNodeId))
        return newNode, isNew

    def buildOutSampleSpace(self, max_sample: int = 500):
        currentSeeds = self.seeds
        newNodes = []
        while len(self.sampleSpace.nodes) < max_sample and len(currentSeeds) > 0:  # run until graph is big or we aren't generating new samples
            for tactic_name in self._tactics:
                for tactic_func in self._tactics[tactic_name]:
                    for seed in currentSeeds:
                        newNode, isNew = self.branch(seed, tactic_name, tactic_func)
                        if isNew:  # new node, new portion of space
                            newNodes.append(newNode)
            currentSeeds = newNodes
            newNodes = []

    def averageImprovement(self, objective_name) -> float:
        raw_diffs = [self.getEdge(u_id, v_id).score_diffs[objective_name] for u_id, v_id in self.sampleSpace.edges]
        min_dif = min(*raw_diffs)
        max_dif = max(*raw_diffs)
        if min_dif == max_dif:  # no change across edges
            return max_dif

        def featureScale(dif):
            if max_dif == min_dif:
                assert dif == max_dif
            return (dif - min_dif) / (max_dif - min_dif)

        scaled_diffs = [featureScale(dif) for dif in raw_diffs]
        total = sum(scaled_diffs)
        average = total / len(raw_diffs)
        return average

    def expectedImprovementByTactic(self, objective_name: str, tactic_name: str) -> float:
        raw_diffs = [self.getEdge(u_id, v_id).score_diffs[objective_name] for u_id, v_id in self.edgesByTactic[tactic_name]]
        min_dif = min(raw_diffs)
        max_dif = max(raw_diffs)
        if min_dif == max_dif:
            if min_dif == 0:
                return 0.0
            return min_dif / float(len(raw_diffs))

        dif_range = (max_dif - min_dif)
        scaled_zero = (0.0 - min_dif) / dif_range

        def scale(dif):
            scaled = (dif - min_dif) / dif_range
            scaled -= scaled_zero
            return scaled

        scaled_diffs = [scale(dif) for dif in raw_diffs]
        total_dif = sum(scaled_diffs)
        return total_dif / float(len(raw_diffs))

    def getTacticToObjectiveWeights(self) -> Dict[CallModifier, Dict[str, float]]:
        weights = {}
        for tactic_name in self._tactics:
            tactic_modifier = CallModifier(self.registry, tactic_name)
            tactic_RawWeights = {}
            for objective_name in self._objectives:
                weight = max(0.0, self.expectedImprovementByTactic(objective_name, tactic_name))
                if weight > 0.0:
                    tactic_RawWeights[objective_name] = weight
            if len(tactic_RawWeights) == 0:
                tactic_RawWeights[tactic_modifier] = {}
            else:
                min_weight = min(tactic_RawWeights.values())
                weights[tactic_modifier] = {obj_name: raw / min_weight for obj_name, raw in tactic_RawWeights.items()}
        return weights

    def storeTacticToObjectiveWeights(self, outFileName: str = "tacticWeights.json"):
        tToOWeights = self.getTacticToObjectiveWeights()
        data = {str(modifier.toJsonDict()): weightDict for modifier, weightDict in tToOWeights.items()}
        with open(outFileName, 'w') as outfile:
            json.dump(data, outfile)

    def tacticOptimizedHeuristicMap(self, objectivesToWeights: Dict[CallModifier, float]) -> HeuristicMap:
        for modifier in objectivesToWeights:
            assert modifier.generalName in self._objectives, f"{modifier.generalName} not considered in sampler"
        tacticsToObjectiveWeights = self.getTacticToObjectiveWeights()
        return HeuristicMap(objectivesToWeights=objectivesToWeights, tacticsToObjectiveWeights=tacticsToObjectiveWeights, registry=self.registry)

    def __contains__(self, nodeID):
        if type(nodeID) == int:
            return nodeID in self.sampleSpace.nodes
        if type(nodeID) == Tuple:
            first_NodeID = nodeID[0]
            second_NodeID = nodeID[1]
            return self.sampleSpace.has_edge(first_NodeID, second_NodeID)
        return False


def buildSampler(seedSampleSize: int, fullSampleSize: int,
                 synthesizer: Callable, registry: ObjectiveTacticRegistry, parameterRanges: Optional[Dict[str, Iterable]] = None) -> TacticObjectiveGraph:
    if parameterRanges is None:
        parameterRanges = {}
    sample = [synthesizer() for _ in range(0, seedSampleSize)]
    objectives = []
    for objectiveName, objectiveParams in registry.objectiveNamesToParameters.items():
        kwargs_ranges = {}
        for paramName in objectiveParams:
            if paramName not in ["self", "raw_score", "_"]:
                if paramName in parameterRanges:
                    kwargs_ranges[paramName] = parameterRanges[paramName]  # range specified by user
                else:
                    kwargs_ranges[paramName] = [objectiveParams[paramName]]  # range of default value
        objectives.append(CallModifierAcrossRange(registry, objectiveName, **kwargs_ranges))
    tactics = set()
    for tacticName, tacticParams in registry.tacticNamesToParameters.items():
        kwargs_ranges = {}
        for paramName in tacticParams:
            if paramName not in ["self", "_"]:
                if paramName in parameterRanges:
                    kwargs_ranges[paramName] = parameterRanges[paramName]  # range specified by user
                else:
                    kwargs_ranges[paramName] = [tacticParams[paramName]]  # range of default value
        tactics.add(CallModifierAcrossRange(registry, tacticName, **kwargs_ranges))
    sampler = TacticObjectiveGraph(registry=registry, seedSamples=sample, objectives=objectives, tactics=tactics)
    sampler.buildOutSampleSpace(fullSampleSize)
    return sampler


def getLearnedTactics(modifier: str, tacticFile: str, registry: ObjectiveTacticRegistry, synthesizer: Callable, fullSampleSize: int = 500, seedSampleSize: int = 1):
    tactics = readTacticsToObjectiveWeightsFromJson(registry, tacticFile)
    if len(tactics) == 0:  # otherwise build from sample
        sampler = buildSampler(seedSampleSize=seedSampleSize, fullSampleSize=fullSampleSize, synthesizer=synthesizer, registry=registry)
        sampler.storeTacticToObjectiveWeights(tacticFile)
        generalTactics = sampler.getTacticToObjectiveWeights()
        tactics = {}
        for tactic, objectives in generalTactics.items():
            modifiedObjectives = {f"{objectiveName}{modifier}": objectives[objectiveName] for objectiveName in objectives}
            tactics[tactic] = modifiedObjectives
    return tactics
