import math
import random

import numpy

from GIST.DesignPopulation import DesignPopulation, DesignIteration


def biasedRandomDesignSelection(designPopulation: DesignPopulation) -> DesignIteration:
    # scoreRanges: Dict[float, float] = {}
    # score_sum = 0
    # for score in designPopulation.designsByScore:
    #     score_sum += score
    #     scoreRanges[score_sum] = score
    #
    # randomVal = random() * score_sum
    # for boundary_val in scoreRanges:
    #     if randomVal <= boundary_val:
    #         chosenScore = scoreRanges[boundary_val]
    #         return designPopulation.getRandomDesignByScore(chosenScore)
    # assert False
    randomScore = numpy.random.normal(loc=designPopulation.getMaxScore(), scale=designPopulation.standardDeviation())
    minDistance = math.inf
    closestScore = -1.0
    for score in designPopulation.designsByScore:
        dist = score - randomScore
        if 0 < dist < minDistance:  # choice the closest but higher score
            closestScore = score
            minDistance = dist
    if closestScore not in designPopulation.designsByScore:
        closestScore, choices = designPopulation.designsByScore.peekitem()  # take the top item if we drew badly from distribution
    design = random.choice(designPopulation.designsByScore[closestScore])
    return design
