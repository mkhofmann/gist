from random import random
from typing import Callable

from GIST.DesignPopulation import DesignIteration
from GIST.HeuristicMap import HeuristicMap


def biasedByExpectedImprovementScore(designIteration: DesignIteration, heuristicMap: HeuristicMap) -> Callable:
    expectedImprovementScores = heuristicMap.expectedImprovementScores(designIteration.design)
    scoreRanges = {}
    boundary = 0
    for tactic_name in expectedImprovementScores:
        eis = expectedImprovementScores[tactic_name]
        if eis > 0.0:
            boundary += eis
            scoreRanges[boundary] = tactic_name

    randomVal = random() * boundary
    for boundary_val in scoreRanges:
        if randomVal <= boundary_val:
            return heuristicMap.getTactic(scoreRanges[boundary_val])
    assert False
