import json
from typing import Dict, Callable, Tuple, Optional, Any, List

from pulp import LpMaximize, LpProblem, LpVariable, lpSum, LpStatus, value

from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry
from GIST.Registry.CallModifier import CallModifier, callModifierFromJson


class HeuristicMap:
    def __init__(self, objectivesToWeights: Dict[CallModifier, float], tacticsToObjectiveWeights: Dict[CallModifier, Dict[str, float]], registry: ObjectiveTacticRegistry):
        self.registry = registry
        self._objectiveNamesToCalls = {}
        self._tacticNamesToCalls = {}
        self.objectiveNamesToWeights: Dict[str, float] = {}
        self.tacticsToObjectiveWeights: Dict[str, Dict[str, float]] = {}
        self.maxPossibleScore = 0.0
        for objectiveCaller, objective_weight in objectivesToWeights.items():
            self.maxPossibleScore += objective_weight  # the top score possible is a perfect score of 1.0 by all weights
            objective_name = objectiveCaller.name()
            objective_func = objectiveCaller.specificCall(isObjective=True)
            self._objectiveNamesToCalls[objective_name] = objective_func
            self.objectiveNamesToWeights[objective_name] = objective_weight
        self.registerSpecializedObjectives()
        for tacticCaller, tactic_weights in tacticsToObjectiveWeights.items():
            tactic_name = tacticCaller.name()
            tactic_func = tacticCaller.specificCall(isObjective=False)
            self._tacticNamesToCalls[tactic_name] = tactic_func
            actual_weights = {}
            for objective_name, weight in tactic_weights.items():
                if objective_name not in self._objectiveNamesToCalls:
                    for actual_objective_name in self._objectiveNamesToCalls:
                        if objective_name in actual_objective_name:  # name is a modified version of this function
                            actual_weights[actual_objective_name] = weight
                else:
                    actual_weights[objective_name] = weight
            self.tacticsToObjectiveWeights[tactic_name] = actual_weights
        self.registerSpecializedTactics()
        self.objectivesToTacticsWeights: Dict[str: Dict[str, float]] = {}  # strings identifiers of objectives that points to the weighted tactics
        self._totalObjectiveWeights: float = 0.0
        self.objectivesToImportance: Dict[str, float] = {}
        self.tacticObjectiveValues: Dict[str, Dict[str, float]] = {}
        self._updateMap()

    def registerSpecializedObjectives(self):
        for objective_name, objective in self._objectiveNamesToCalls.items():
            self.registry.addObjective(objective_name, objective, None)

    def registerSpecializedTactics(self):
        for tactic_name, tactic in self._tacticNamesToCalls.items():
            self.registry.addTactic(tactic_name, tactic)

    def _updateMap(self):
        self.objectivesToTacticsWeights: Dict[str: Dict[str, float]] = {}  # strings identifiers of objectives that points to the weighted tactics
        for tactic_name in self.tacticsToObjectiveWeights:
            for objective_name in self.tacticsToObjectiveWeights[tactic_name]:
                if objective_name not in self.objectivesToTacticsWeights:
                    self.objectivesToTacticsWeights[objective_name] = {}
                self.objectivesToTacticsWeights[objective_name][tactic_name] = self.tacticsToObjectiveWeights[tactic_name][objective_name]
        self._totalObjectiveWeights: float = self._sumObjectiveWeights()
        self.objectivesToImportance: Dict[str, float] = self._generalObjectiveImportance()
        self.tacticObjectiveValues: Dict[str, Dict[str, float]] = self._getTacticObjectiveValues()

    def addTactics(self, additionalTacticNamesToCalls: Dict[str, Callable], additionalTacticsToObjectiveWeights: Dict[str, Dict[str, float]]):
        self._tacticNamesToCalls.update(additionalTacticNamesToCalls)
        self.tacticsToObjectiveWeights.update(additionalTacticsToObjectiveWeights)
        self._updateMap()

    def addObjective(self, additionalObjectiveNamesToCalls: Dict[str, Callable], additionalObjectiveToWeights: Dict[str, float]):
        # for name, objective in additionalObjectiveNamesToCalls.items():
        #     self._objectiveNamesToCalls[name] = objective
        self._objectiveNamesToCalls.update(additionalObjectiveNamesToCalls)
        self.objectiveNamesToWeights.update(additionalObjectiveToWeights)
        self._updateMap()

    def getObjective(self, objectiveName: str) -> Callable:
        return self._objectiveNamesToCalls.get(objectiveName)

    def getTactic(self, tacticName: str) -> Callable:
        return self._tacticNamesToCalls.get(tacticName)

    def expectedImprovementScores(self, design: Any) -> Dict[str, float]:
        """
            Helps to estimate how much a particular tactic will improve a given design
        @param design: whatever is beign modified by this tactic
        @return: a dictionary that keys the tactic id to the expected improvement score which is the tactic's value to each objective times the objectives importance
        """
        objectiveImportance = self._relativeObjectiveImportance(design)
        tacticsExpectedImprovementScores = {}
        for tactic_name in self.tacticsToObjectiveWeights:
            # sumVal = 0
            # for objective in self.tacticsToObjectiveWeights[tactic]:
            #     value = self.tacticObjectiveValues[tactic][objective]
            #     importance = objectiveImportance[objective]
            #     sumVal += value*importance
            # ^^ equivilant loop
            sumVal = sum(
                [self.tacticObjectiveValues[tactic_name][objective_name] * objectiveImportance[objective_name] for objective_name in
                 self.tacticsToObjectiveWeights[tactic_name]])
            tacticsExpectedImprovementScores[tactic_name] = sumVal
        return tacticsExpectedImprovementScores

    def _tacticTotalValue(self, tactic_name: str) -> float:
        """sum of all weights for this tactic across all relavent objectives"""
        return sum(self.tacticsToObjectiveWeights[tactic_name][objective_name] for objective_name in self.tacticsToObjectiveWeights[tactic_name])

    def _getTacticObjectiveValues(self) -> Dict[str, Dict[str, float]]:
        """V(t, o) = weight of tactic over tacticTotalValue
        @return: dictionary keyed by tactic ids to sub dictionaries of objective ids and the value of that tactic for the objective
        """
        values = {}
        for tactic_name in self._tacticNamesToCalls:
            values[tactic_name] = {}
            for objective_name in self.tacticsToObjectiveWeights[tactic_name]:
                total_tacticValue_for_objective = sum(self.objectivesToTacticsWeights[objective_name][subTactic] for subTactic in self.objectivesToTacticsWeights[objective_name])
                values[tactic_name][objective_name] = self.tacticsToObjectiveWeights[tactic_name][objective_name] / total_tacticValue_for_objective
        return values

    def _sumObjectiveWeights(self) -> float:
        """sum of the weights of all objectives"""
        return sum([self.objectiveNamesToWeights[objective_name] for objective_name in self.objectiveNamesToWeights])

    def _generalObjectiveImportance(self) -> Dict[str, float]:
        """I(o in O) = weight_o / sum of all weights
        @return: dictionary keyed by the objective id to the objective's importance
        """
        objectiveValues = {}
        for objective_name in self.objectiveNamesToWeights:
            objectiveValues[objective_name] = self.objectiveNamesToWeights[objective_name] / self._totalObjectiveWeights
        return objectiveValues

    def _relativeObjectiveImportance(self, design: Any) -> Dict[str, float]:
        """I(o in O, s_o) = weight_o * (1.0-s_o) / sum of all weights by scores
        @param design: the design being evaluated
        @return: dictionary keyed by the objective id tot he objectives importance for this particular design
        """
        objectiveValues = {}
        objectivesToScores = self.collectScores(design)
        totalImportance = self._sumObjectiveWeightsByScore(objectivesToScores)
        for objective_name in self.objectiveNamesToWeights:
            objective_weight = self.objectiveNamesToWeights[objective_name]
            objective_score = objectivesToScores[objective_name]
            weightedScoreDifference = (objective_weight * (1.0 - objective_score))
            if totalImportance == 0:
                assert weightedScoreDifference == 0.0, "The importance of this objective is 0"
                objectiveValues[objective_name] = 1.0
            else:
                objectiveValues[objective_name] = weightedScoreDifference / totalImportance
        return objectiveValues

    def _sumObjectiveWeightsByScore(self, objectivesToScores: Dict[str, float]) -> float:
        """
        @param objectivesToScores: dictionary of objective ids to the current scores
        @return: sum of all weights adjusted by score
        """
        # sumVal = 0
        # for objective in self.objectivesToWeight:
        #     weight = self.objectivesToWeight[objective]
        #     score = objectivesToScores[objective]
        #     sumVal += weight * (1.0 - score)
        # equivilant for loop ^^
        sumVal = sum([self.objectiveNamesToWeights[objective_name] * (1.0 - objectivesToScores[objective_name])
                      for objective_name in self.objectiveNamesToWeights])  # sum of { weight_o * (1 - score_o)
        return sumVal

    def collectScores(self, design: Any) -> Dict[str, float]:
        """
        @param design: the design being considered
        @return: a dictionary keyed by objective names to the current score of this design
        """
        objectivesToScores = {}
        for objective_name in self.objectiveNamesToWeights:
            objectivesToScores[objective_name] = self.getObjective(objective_name)(design)
        return objectivesToScores

    def scoreDesign(self, design: Any) -> float:
        """
        @param design: the design being considered
        @return: the total weighted score of this design across all objectives
        """
        scores = {objective_name: (self.getObjective(objective_name)(design) * self.objectiveNamesToWeights[objective_name]) for objective_name in self.objectiveNamesToWeights}
        score = sum(scores.values())
        return score

    def __str__(self):
        return str(self.tacticsToObjectiveWeights)


def minimizeHeuristicMap(baseMap: HeuristicMap, registry:ObjectiveTacticRegistry, max_num_tactics_per_objective: Optional[int] = None, max_tactic_usage: Optional[int] = None,
                         conflictingTactics: Optional[List[List[str]]] = None) -> Tuple[HeuristicMap, bool]:
    """
    Creates an optimized heuristic map that minimizes the randomness introduced by using a wide range of tactics for each objective
    @param conflictingTactics: a list of sets of tactics that cannot be used simultaneously
    @param baseMap: The heuristic map to be copied and optimized
    @param max_num_tactics_per_objective:
    @param max_tactic_usage:
    @return: Return a heuristic map which makes efficient use of tactics subject to constraints
    :param registry: The ObjectiveTacticRegistry that stores the common calls for this GIST optimization
    """
    if conflictingTactics is None:
        conflictingTactics = []

    def pairKey(obj: str, tact: str):
        return f"{obj}->{tact})"

    if max_num_tactics_per_objective is None:  # default to the maximum number of tactics for an objective, effectively ignoring this constraint
        max_num_tactics_per_objective = max([len(baseMap.objectivesToTacticsWeights[objective])
                                             for objective in baseMap.objectivesToTacticsWeights])

    if max_tactic_usage is None:  # default to the maximum number of objectives, effectively ignoring this constraint
        max_tactic_usage = len(baseMap.objectiveNamesToWeights)

    objectiveToTacticNames = []
    objectiveToTacticPairs = []
    for objective_name in baseMap.objectivesToTacticsWeights:
        for tactic_name in baseMap.objectivesToTacticsWeights[objective_name]:
            objectiveToTacticNames.append(pairKey(objective_name, tactic_name))
            objectiveToTacticPairs.append((objective_name, tactic_name))
    problem = LpProblem("EfficientHeuristicMap", LpMaximize)
    objective_tactic_pairs = LpVariable.dicts("Pair", objectiveToTacticNames, cat="Binary")
    # dictionary of variable Strings "Pair_objectiveName_tacticName" which result in binary values of 0 or 1
    # if a pair has a 1 value, it should be included in the minimized heuristic map
    # objective function: maximize expectedImprovementScores
    problem += lpSum([(objective_tactic_pairs[pairKey(pair[0], pair[1])] *
                       baseMap.objectivesToImportance[pair[0]] *
                       baseMap.tacticObjectiveValues[pair[1]][pair[0]])
                      for pair in objectiveToTacticPairs]), "Total_Value"

    # constraints:
    # constraint 1: maximum number of tactics per objective
    # constraint 2: each objective has at least 1 tactic
    # constraint 3: each tactic is used a maximum number of times
    for objective_name in baseMap.objectiveNamesToWeights:
        problem += lpSum([objective_tactic_pairs[pairKey(objective_name, tactic_name)]
                          for tactic_name in baseMap.objectivesToTacticsWeights[objective_name]]) <= max_num_tactics_per_objective, \
                   f"Max {max_num_tactics_per_objective} tactics for {str(objective_name)} "
        problem += lpSum([objective_tactic_pairs[pairKey(objective_name, tactic)]
                          for tactic in baseMap.objectivesToTacticsWeights[objective_name]]) >= 1, f"{str(objective_name)}  has at least 1 tactic"
    for tactic_name in baseMap.tacticsToObjectiveWeights:
        problem += lpSum([objective_tactic_pairs[pairKey(objective, tactic_name)] for objective in
                          baseMap.tacticsToObjectiveWeights[
                              tactic_name]]) <= max_tactic_usage, f"{tactic_name} is used maximum {max_tactic_usage} times"

    # bonus constraints for conflicting tactics:
    if len(conflictingTactics) > 0:
        for conflictList in conflictingTactics:
            conflictingPairs = []
            for objective_name in baseMap.objectiveNamesToWeights:
                for tactic_name in conflictList:
                    if tactic_name in baseMap.objectivesToTacticsWeights[objective_name]:
                        conflictingPairs.append(pairKey(objective_name, tactic_name))
            problem += lpSum([objective_tactic_pairs[pair] for pair in conflictingPairs]) <= 1, f"{conflictList} does not overlap"

    problem.solve()
    print("Status:", LpStatus[problem.status])
    if problem.status != 1:
        return baseMap, False

    finalObjectivesToWeight: Dict[CallModifier, float] = {}
    finalTacticsToObjectivesToWeights: Dict[CallModifier, Dict[str, float]] = {}
    for pair_name, pair in zip(objectiveToTacticNames, objectiveToTacticPairs):
        objective_name = pair[0]
        objective_modifier = CallModifier(registry, objective_name, "_minimized")
        tactic_name = pair[1]
        tactic_modifier = CallModifier(registry, objective_name, "_minimized")
        pair_value = value(objective_tactic_pairs[pair_name])
        if pair_value == 1.0:
            if tactic_modifier not in finalTacticsToObjectivesToWeights:
                finalTacticsToObjectivesToWeights[tactic_modifier] = {}
            finalTacticsToObjectivesToWeights[tactic_modifier][objective_name] = baseMap.tacticsToObjectiveWeights[tactic_name][objective_name]
            finalObjectivesToWeight[objective_modifier] = baseMap.objectiveNamesToWeights[objective_name]
        else:
            assert pair_value == 0.0
    heuristic_map = HeuristicMap(objectivesToWeights=finalObjectivesToWeight, tacticsToObjectiveWeights=finalTacticsToObjectivesToWeights, registry=registry)
    return heuristic_map, True


def readTacticsToObjectiveWeightsFromJson(registry, json_filename: str = "tacticWeights.json"):
    tToOWeights = {}
    try:
        with open(json_filename) as json_file:
            data = json.load(json_file)
            for key, weights in data.items():
                keyData = json.loads(key)
                modifier = callModifierFromJson(registry, keyData["CallModifier"])
                tToOWeights[modifier] = weights
    except IOError:
        print(f"Tactic file \"{json_filename}\" is not accessible")
        return {}
    return tToOWeights
