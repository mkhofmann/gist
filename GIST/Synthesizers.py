import random
from typing import List, Any


def synthesizeFromSamples(samples: List[Any]):
    return random.choice(samples)


def synthesizeFromSampleAndReduceSet(samples: List[Any]):
    index = random.choice(range(0, len(samples)))
    return samples.pop(index)
