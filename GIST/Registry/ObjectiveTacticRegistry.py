import functools
import inspect
from typing import Dict, Callable, Any, Optional


class ObjectiveTacticRegistry:
    def __init__(self):
        self._objectiveNamesToCalls: Dict[str, Callable] = {}
        self._objectiveNamesToParameters: Dict[str, Dict[str, Any]] = {}
        self._tacticNamesToCalls: Dict[str, Callable] = {}
        self._tacticNamesToParameters: Dict[str, Dict[str, Any]] = {}

    def addObjective(self, objective_name: str, objective: Callable, evaluationFunction: Optional[Callable]):
        self._objectiveNamesToCalls[objective_name] = objective
        parameters = {}
        objective_sig = inspect.signature(objective)
        for parameterName in objective_sig.parameters:
            if parameterName not in ["self", "_"]:
                parameter = objective_sig.parameters[parameterName]
                # noinspection PyUnresolvedReferences,PyProtectedMember
                if not isinstance(parameter.default, inspect._empty):
                    parameters[parameterName] = parameter.default
                elif parameter.annotation is float or parameter.annotation is int:
                    parameters[parameterName] = 1.0
                elif parameter.annotation is bool:
                    parameters[parameterName] = True
                else:
                    parameters[parameterName] = ""
        if evaluationFunction is not None:
            evaluation_sig = inspect.signature(evaluationFunction)
            for parameterName in evaluation_sig.parameters:
                if parameterName not in ["raw_score", "_"]:
                    parameter = evaluation_sig.parameters[parameterName]
                    # noinspection PyUnresolvedReferences
                    if type(parameter.default) in [bool, int, str, float]:
                        parameters[parameterName] = parameter.default
                    elif parameter.annotation is float or parameter.annotation is int:
                        parameters[parameterName] = 1.0
                    elif parameter.annotation is bool:
                        parameters[parameterName] = True
                    else:
                        parameters[parameterName] = ""
        self._objectiveNamesToParameters[objective_name] = parameters

    def addTactic(self, tactic_name: str, tactic: Callable):
        self._tacticNamesToCalls[tactic_name] = tactic
        parameters = {}
        tactic_sig = inspect.signature(tactic)
        for parameterName in tactic_sig.parameters:
            if parameterName not in ["self", "_"]:
                parameter = tactic_sig.parameters[parameterName]
                # noinspection PyUnresolvedReferences,PyProtectedMember
                if not isinstance(parameter.default, inspect._empty):
                    parameters[parameterName] = parameter.default
                elif parameter.annotation is float or parameter.annotation is int:
                    parameters[parameterName] = 1.0
                elif parameter.annotation is bool:
                    parameters[parameterName] = True
                else:
                    parameters[parameterName] = ""
        self._tacticNamesToParameters[tactic_name] = parameters

    def getObjective(self, objective_name: str, *specific_args, **specific_kwargs) -> Callable:
        """
        Gathers this objective from the objective registry and returns it with applied args
        :param objective_name: the name of the objective in the registry
        :param specific_args: additional args to be applied to the objective function
        :param specific_kwargs: additional keyed args to be applied to the objective
        :return: an objective function that only takes in the design and outputs a score between 0 and 1
        """
        assert objective_name in self._objectiveNamesToCalls
        objective = self._objectiveNamesToCalls[objective_name]
        if len(specific_args) == 0 and len(specific_kwargs) == 0:
            return objective

        @functools.wraps(objective)
        def specific_objective(design, **_) -> float:
            return objective(design, *specific_args, **specific_kwargs)

        return specific_objective

    def getTactic(self, tactic_name: str, *specific_args, **specific_kwargs) -> Callable:
        """
        Gathers this tactic from the tactic registry and returns it with applied args
        :param tactic_name: the name of the tactic in the registry
        :param specific_args: additional args to be applied to the objective function
        :param specific_kwargs: additional keyed args to be applied to the objective
        :return: an tactic function that only takes in the design and outputs a new design
        """
        assert tactic_name in self._tacticNamesToCalls
        tactic = self._tacticNamesToCalls[tactic_name]
        if len(specific_args) == 0 and len(specific_kwargs) == 0:
            return tactic

        @functools.wraps(tactic)
        def specific_tactic(design):
            return tactic(design, *specific_args, **specific_kwargs)

        return specific_tactic

    @property
    def tacticNamesToCalls(self):
        return self._tacticNamesToCalls

    @property
    def objectiveNamesToCalls(self) -> Dict[str, Callable]:
        return self._objectiveNamesToCalls

    @property
    def objectiveNamesToParameters(self) -> Dict[str, Dict[str, Any]]:
        return self._objectiveNamesToParameters

    @property
    def tacticNamesToParameters(self) -> Dict[str, Dict[str, Any]]:
        return self._tacticNamesToParameters

    def setObjectiveParameterDefault(self, objectiveName: str, parameterName: str, value: Any):
        self._objectiveNamesToParameters[objectiveName][parameterName] = value
