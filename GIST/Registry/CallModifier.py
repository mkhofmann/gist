import inspect
import json
from typing import Dict, Any, List, Iterable, Callable

from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry


class CallModifier:
    def __init__(self, registry: ObjectiveTacticRegistry, generalName: str, nameModifier: str = "", **specific_kwargs):
        self.registry = registry
        self.generalName = generalName
        self.nameModifier = nameModifier
        self.specificKwargs = specific_kwargs

    def name(self) -> str:
        return self.generalName + self.nameModifier

    def specificCall(self, isObjective: bool):
        if isObjective:
            general_func = self.registry.getObjective(self.generalName, **self.specificKwargs)
        else:
            general_func = self.registry.getTactic(self.generalName, **self.specificKwargs)

        return general_func

    def __hash__(self):
        return hash(self.name())

    def __str__(self):
        return self.name()

    def __repr__(self):
        return self.name()

    def toJsonDict(self) -> str:
        result = {"generalName": self.generalName, "nameModifier": self.nameModifier}
        for key, arg in self.specificKwargs:
            assert type(arg) == str or type(arg) == float or type(arg) == bool or arg is None, f"{key}:{arg} cannot be stored in JSON"
            result[key] = arg
        data = {"CallModifier": result}
        return json.dumps(data)


class CallModifierAcrossRange:
    def __init__(self, registry: ObjectiveTacticRegistry, generalName, **kwarg_ranges):
        self.registry = registry
        self.generalName = generalName
        self.kwarg_ranges = kwarg_ranges

    def modifiersOverRange(self) -> List[CallModifier]:
        if len(self.kwarg_ranges) == 0:
            return [CallModifier(self.registry, self.generalName)]

        def argSets(**keys_domains: Dict[str, Iterable[Any]]) -> List[Dict[str, Any]]:
            other_keys = {}
            key = None
            domain = []
            for iteration, k in enumerate(keys_domains):
                if iteration == 0:
                    key = k
                    domain = keys_domains[k]
                else:
                    other_keys[k] = keys_domains[k]
            if len(other_keys) > 0:
                completedSets = argSets(**other_keys)
            else:
                completedSets = None

            key_args = [{key: arg} for arg in domain]
            if completedSets is None:
                return key_args
            else:
                completedKwargs = []
                for kwarg in key_args:
                    for otherKeyArgs in completedSets:
                        newKwargs = {}
                        newKwargs.update(kwarg)
                        newKwargs.update(otherKeyArgs)
                        completedKwargs.append(newKwargs)
                return completedKwargs

        modifiers = []
        allKwargs = argSets(**self.kwarg_ranges)
        for i, kwargs in enumerate(allKwargs):
            modifiers.append(CallModifier(self.registry, self.generalName, f"_{i}", **kwargs))

        return modifiers


def callModifierFromJson(registry: ObjectiveTacticRegistry, jsonDict: Dict[str, Any]) -> CallModifier:
    return CallModifier(registry, **jsonDict)


def keptKeyedArgs(func: Callable, kwargs) -> Dict[str, Any]:
    signature = inspect.signature(func)
    kept = {}
    for key, value in kwargs.items():
        if key in signature.parameters:
            kept[key] = value
    return kept
