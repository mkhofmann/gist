import copy
import functools
from typing import Callable

from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry


def tactic(tacticName: str, registry: ObjectiveTacticRegistry) -> Callable:
    """
    Used to decorate class methods to register them as tactics
    :param registry:
    :param tacticName:
    :return: returns the decorated function
    """

    def decorator_addTactic(func: Callable) -> Callable:
        """
        the internal decorator that can access the functions variables and the tactic functions variables
        :param func: the function being decorated
        :return: a decorated function
        """
        registry.addTactic(tacticName, func)

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return decorator_addTactic


def method_tactic(tacticName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def decorator_addTactic(func: Callable) -> Callable:
        @functools.wraps(func)
        def newTactic(design, **kwargs):
            copyDesign = copy.deepcopy(design)
            _ = func(copyDesign, **kwargs)
            return copyDesign

        registry.addTactic(tacticName, newTactic)

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return decorator_addTactic


def modifyAndCopy_Tactic(tacticName: str, modificationFunction: Callable, registry: ObjectiveTacticRegistry) -> Callable:
    """
    Registers a tactic that modifies the output of the
    :param registry:
    :param tacticName: The name of the tactic being registered
    :param modificationFunction: The function being applied to a copy of a incoming design. Must take one argument (design) and return something of the same type
    :return: the decorated function
    """

    def decorator_addTactic(func: Callable) -> Callable:
        """
        the internal decorator that can access the functions variables and the tactic functions variables
        :param func: the function being decorated
        :return: a decorated function
        """

        @functools.wraps(func)
        def newTactic(design):
            copyDesign = copy.deepcopy(design)
            modifiedDesign = modificationFunction(copyDesign)
            return modifiedDesign

        registry.addTactic(tacticName, newTactic)

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return decorator_addTactic


def modifyPropertyAndCopy_Tactic(tacticName: str, modificationFunction: Callable, registry):
    """
    Registers a tactic that modifies the output of the
    :param registry:
    :param tacticName: The name of the tactic being registered
    :param modificationFunction: The function being applied to a copy of a incoming design. Must take one argument (the property value of design) and return something of the same type
    :return: the decorated function
    """

    def decorator_addTactic(prop: property) -> property:
        @functools.wraps(prop.fget)
        def newTactic(design):  # todo this needs to be checked thoroughly
            designCopy = copy.deepcopy(design)
            currentValue = prop.fget(self=design)  # warning because we are modifying the self variable
            modifiedValue = modificationFunction(currentValue)
            prop.fset(self=designCopy, value=modifiedValue)
            return designCopy

        registry.addTactic(tacticName, newTactic)

        return prop

    return decorator_addTactic


def incrementProperty_tactic(increment: float, tacticName: str, registry: ObjectiveTacticRegistry):
    return modifyPropertyAndCopy_Tactic(tacticName=tacticName, modificationFunction=lambda curVal, inc=increment: curVal + inc, registry=registry)


def setPropertyValue_tactic(newValue, tacticName: str, registry: ObjectiveTacticRegistry):
    return modifyPropertyAndCopy_Tactic(tacticName=tacticName, modificationFunction=lambda _, val=newValue: val, registry=registry)
