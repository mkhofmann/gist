import functools
import inspect
from collections import Callable
from typing import Any, Dict, List, Optional

from GIST.Registry.CallModifier import keptKeyedArgs
from GIST.Registry.ObjectiveTacticRegistry import ObjectiveTacticRegistry


def parabolicEvaluation(raw_score: float, targetValue: float, distance_bound: float):
    parabolicWidth = -1 / (distance_bound ** 2)
    score = (parabolicWidth * (raw_score - targetValue) ** 2) + 1.0  # an inverted parabola that peaks at 1.0 and intercepts the x axis (distance_bound away)
    bound_score = max(score, 0.0)
    assert bound_score <= 1.0
    return bound_score


def objective(objectiveName: str, registry: ObjectiveTacticRegistry, evaluationFunction: Optional[Callable] = None) -> Callable:
    def evaluate(raw_score, **_):
        return raw_score

    if evaluationFunction is None:
        evaluationFunction = evaluate

    def decorator_objective(func):

        if isinstance(func, property):
            @functools.wraps(func.fget)
            def newObjective_Property(design, **kwargs) -> float:
                raw_objectiveValue = func.fget(design)
                return evaluationFunction(raw_objectiveValue, **kwargs)

            objectiveFunction = newObjective_Property
        else:
            @functools.wraps(func)
            def newObjective(*args, **kwargs) -> float:
                inspect.signature(func)
                obj_kwargs = keptKeyedArgs(func, kwargs)
                evaluation_kwargs = keptKeyedArgs(evaluationFunction, kwargs)
                raw_objectiveValue = func(*args, **obj_kwargs)
                evaluation = evaluationFunction(raw_objectiveValue, **evaluation_kwargs)
                return evaluation

            objectiveFunction = newObjective

        registry.addObjective(objectiveName, objectiveFunction, evaluationFunction)

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        if isinstance(func, property):
            return func  # return the property instead
        else:
            return wrapper  # return the un modified wrapped function

    return decorator_objective


def equivalentToValue(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: Any, targetValue: Any, **_):
        return float(raw_score == targetValue)

    return objective(objectiveName, registry, evaluation)


def approachesValue(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, targetValue: float, distance_bound: float):
        """
        Return a score that approaches 1 as the raw_score appraoches the target using a gaussian curve
        https://stats.stackexchange.com/questions/143631/height-of-a-normal-distribution-curve
        :param distance_bound:
        :param targetValue:
        :param raw_score:
        """
        return parabolicEvaluation(raw_score, targetValue, distance_bound)

    return objective(objectiveName, registry, evaluation)


def lessThanOrEqualToValue(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, targetValue: float, **_):
        return float(raw_score <= targetValue)

    return objective(objectiveName, registry, evaluation)


def greaterThanOrEqualToValue(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, targetValue: float, **_):
        return float(raw_score >= targetValue)

    return objective(objectiveName, registry, evaluation)


def withinBounds(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: Any, minimumValue: float, maximumValue: float, **_):
        return float(minimumValue <= raw_score <= maximumValue)

    return objective(objectiveName, registry, evaluation)


def ourSideBounds(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, minimumValue: float, maximumValue: float, **_):
        return float(minimumValue >= raw_score >= maximumValue)

    return objective(objectiveName, registry, evaluation)


def approachingOrGreaterThanValue_objective(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, targetValue: float, distance_bound: float):
        if raw_score >= targetValue:
            return 1.0
        else:
            return parabolicEvaluation(raw_score, targetValue, distance_bound)

    return objective(objectiveName, registry, evaluation)


def approachingOrLessThanValue_objective(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, targetValue: float, distance_bound: float):
        if raw_score <= targetValue:
            return 1.0
        else:
            return parabolicEvaluation(raw_score, targetValue, distance_bound)

    return objective(objectiveName, registry, evaluation)


def approachingAndGreaterThanValue(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, targetValue: float, distance_bound: float):
        if raw_score < targetValue:
            return 0.0
        else:
            return parabolicEvaluation(raw_score, targetValue, distance_bound)

    return objective(objectiveName, registry, evaluation)


def approachingAndLessThanValue(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: float, targetValue: float, distance_bound: float):
        if raw_score > targetValue:
            return 0.0
        else:
            return parabolicEvaluation(raw_score, targetValue, distance_bound)

    return objective(objectiveName, registry, evaluation)


def preferredChoice(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: Any, choices: Dict[Any, float], **_) -> float:
        """
        Gives the the portion of the maximum choice's weight represented by the raw_score
        :param choices:
        :param raw_score:
        :return:
        """
        if raw_score not in choices:
            return 0.0
        else:
            return choices[raw_score]

    return objective(objectiveName, registry, evaluation)


def preferredChoices(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score, choices: Dict[Any, float], **_) -> float:
        """
        Returns the portion of the top score recieved by all choices in an iterable raw_score
        @param raw_score: an iterable of choices
        @param choices: a dictionary of choices mapped to their respective weights
        @param _: a place holder for additional objective parameters
        @return: a value between 0 and 1 showing portion of top weight recieved
        """
        choice_sum = 0.0
        topChoiceWeight = max(*choices.values())
        for choice in raw_score:
            if raw_score in choices:
                choice_sum += choices[choice] / topChoiceWeight
        return choice_sum / len(raw_score)

    return objective(objectiveName, registry, evaluation)


def amongChoices(objectiveName: str, registry: ObjectiveTacticRegistry) -> Callable:
    def evaluation(raw_score: Any, choices: List[Any], **_) -> float:
        """
        :param choices:
        :param raw_score:
        :return: return 1.0 if raw_score is among choices
        """
        return float(raw_score in choices)

    return objective(objectiveName, registry, evaluation)
