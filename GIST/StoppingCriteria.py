import math
from typing import List

from GIST.DesignPopulation import DesignIteration, DesignPopulation


def thresholdScoreReached(_: DesignIteration, designPopulation, thresholdPercentMaxScore: float):
    maxPossibleScore = designPopulation.heuristicMap.maxPossibleScore
    thresholdScore = thresholdPercentMaxScore * maxPossibleScore
    return designPopulation.getMaxScore() >= thresholdScore


def best_of_N(designIteration: DesignIteration, _: DesignPopulation, iterationMax=1000):
    return designIteration.iteration >= iterationMax


last_N_steps: List[float] = []
last_N_stepSum: float = 0.0


def minimalImprovement(designIteration: DesignIteration, designPopulation: DesignPopulation, requiredImprovement: float = 1, nSteps: int = 10) -> bool:
    global last_N_stepSum
    global last_N_steps
    resetStepWindow(designPopulation)
    step = designPopulation.getStep(designIteration)
    last_N_steps.append(step)
    last_N_stepSum += step
    if len(last_N_steps) <= nSteps:
        return False
    else:
        firstStep = last_N_steps.pop(0)
        last_N_stepSum -= firstStep  # remove from sum to adjust moving mean window
        meanStep = last_N_stepSum / float(len(last_N_steps))
        return -1.0 * requiredImprovement <= meanStep <= requiredImprovement


def minimalDeviation(_: DesignIteration, designPopulation: DesignPopulation, minDeviation: float = 1.0) -> bool:
    if designPopulation.popCount < designPopulation.populationCap:
        return False  # don't stop without enough samples
    meanScore = sum(designPopulation.scoreByDesign.values()) / float(len(designPopulation.scoreByDesign))
    varianceSum = sum((score - meanScore) ** 2 for score in designPopulation.scoreByDesign.values())
    varianceFraction = varianceSum / (float(len(designPopulation.scoreByDesign) - 1))
    variance = math.sqrt(varianceFraction)
    return variance <= minDeviation


def resetStepWindow(designPopulation: DesignPopulation):
    global last_N_stepSum
    global last_N_steps
    if len(designPopulation) == 0:
        last_N_stepSum = 0.0
        last_N_steps = []


def minimalImprovementByMean(designIteration: DesignIteration, designPopulation: DesignPopulation, portionOfTotalMean: float = .1, nSteps: int = 10) -> bool:
    global last_N_stepSum
    global last_N_steps
    resetStepWindow(designPopulation)
    step = abs(designPopulation.getStep(designIteration))
    last_N_steps.append(step)
    last_N_stepSum += step
    if len(last_N_steps) <= nSteps:
        return False
    else:
        firstStep = last_N_steps.pop(0)
        last_N_stepSum -= firstStep  # remove from sum to adjust moving mean window
        meanStep = last_N_stepSum / float(len(last_N_steps))
        fullMean = designPopulation.meanScore()
        portionMean = fullMean * portionOfTotalMean
        return meanStep <= portionMean
