import random
from unittest import TestCase

from GIST.Demos.GuesserDemo.guesser import Guesser
from GIST.Demos.GuesserDemo.random_number import randomNumber
from GIST.GISTOptimizer import GISTOptimizer
from GIST.HeuristicMap import HeuristicMap, readTacticsToObjectiveWeightsFromJson
from GIST.Registry.CallModifier import CallModifier, CallModifierAcrossRange
from GIST.Selectors.DesignSelectors import biasedRandomDesignSelection
from GIST.Selectors.TacticSelectors import biasedByExpectedImprovementScore
from GIST.StoppingCriteria import thresholdScoreReached
from GIST.TacticObjectiveWeighting.TacticObjectiveGraph import TacticObjectiveGraph


class TestTacticObjectiveGraph(TestCase):

    @staticmethod
    def getSampler(sampleSize=10) -> TacticObjectiveGraph:
        guesses = [Guesser() for _ in range(1, sampleSize)]
        objectives = [CallModifierAcrossRange(Guesser.Guesser_Registry, "greaterThanGuess", targetValue=range(1, 101), distance_bound=[50.0]),
                      CallModifierAcrossRange(Guesser.Guesser_Registry, "lessThanGuess", targetValue=range(1, 101), distance_bound=[50.0])]
        sampler = TacticObjectiveGraph(Guesser.Guesser_Registry, seedSamples=guesses, objectives=objectives,
                                       tactics={CallModifierAcrossRange(Guesser.Guesser_Registry, "increment_guess"),
                                                CallModifierAcrossRange(Guesser.Guesser_Registry, "decrement_guess")},
                                       filters=[Guesser.filterGuessByRange])
        sampler.buildOutSampleSpace(110)
        return sampler

    def test_build_out_sample_space(self):
        sampler = self.getSampler()
        assert len(sampler.sampleSpace.nodes) == 100, f"100 != {len(sampler.sampleSpace.nodes)}"
        print(f"Converges with {len(sampler.sampleSpace.nodes)} nodes")

    def test_total_improvement(self):
        sampler = self.getSampler()
        eI = sampler.expectedImprovementByTactic("greaterThanGuess", "increment_guess")
        eD = sampler.expectedImprovementByTactic("greaterThanGuess", "decrement_guess")
        print(f"greaterThanGuess: increment = {eI}, decrement = {eD}")
        eI = sampler.expectedImprovementByTactic("lessThanGuess", "increment_guess")
        eD = sampler.expectedImprovementByTactic("lessThanGuess", "decrement_guess")
        print(f"lessThanGuess: increment = {eI}, decrement = {eD}")

    def test_get_tactic_to_objective_weights(self):
        sampler = self.getSampler()
        random.seed(1)
        for i in range(0, 10):
            actual = randomNumber()
            modifier = f"_{actual}"  # Note objective names must be modified to ensure call of correct objective with specific arguments (i.e, targetValue)
            # Generate 10 samples between 0 and 100 then iterate until a perfect score of 2.0 is reached
            objectiveWeights = {CallModifier(Guesser.Guesser_Registry, "greaterThanGuess", modifier, targetValue=actual, distance_bound=50): 1.0,
                                CallModifier(Guesser.Guesser_Registry, "lessThanGuess", modifier, targetValue=actual, distance_bound=50): 1.0}
            heuristic_map = sampler.tacticOptimizedHeuristicMap(objectiveWeights)
            optimizer = GISTOptimizer(synthesizer=Guesser,
                                      filters=[Guesser.filterGuessByRange],
                                      heuristicMap=heuristic_map,
                                      designSelector=biasedRandomDesignSelection,
                                      stoppingCriteria=lambda dI, dP: thresholdScoreReached(_=dI, designPopulation=dP, thresholdPercentMaxScore=2.0),
                                      tacticSelector=biasedByExpectedImprovementScore,
                                      startingDesigns=10)
            results = optimizer.runOptimizer()
            finalGuess = results[1][0].design.guess
            print(f"After {results[2].designCount} guess, we guessed {actual} == {finalGuess}")
            assert finalGuess == actual

    def test_store_tactic_to_objective_weights(self):
        sampler = self.getSampler()
        sampler.storeTacticToObjectiveWeights()
        tToOWeights = readTacticsToObjectiveWeightsFromJson(Guesser.Guesser_Registry)
        # random.seed(1)
        for i in range(0, 10):
            actual = randomNumber()
            modifier = f"_{actual}"  # Note objective names must be modified to ensure call of correct objective with specific arguments (i.e, targetValue)
            # Generate 10 samples between 0 and 100 then iterate until a perfect score of 2.0 is reached
            objectiveWeights = {CallModifier(Guesser.Guesser_Registry, "greaterThanGuess", modifier, targetValue=actual, distance_bound=50): 1.0,
                                CallModifier(Guesser.Guesser_Registry, "lessThanGuess", modifier, targetValue=actual, distance_bound=50): 1.0}
            heuristic_map = HeuristicMap(objectiveWeights, tToOWeights, Guesser.Guesser_Registry)
            optimizer = GISTOptimizer(synthesizer=Guesser,
                                      filters=[Guesser.filterGuessByRange],
                                      heuristicMap=heuristic_map,
                                      designSelector=biasedRandomDesignSelection,
                                      stoppingCriteria=lambda dI, dP: thresholdScoreReached(_=dI, designPopulation=dP, thresholdPercentMaxScore=2.0),
                                      tacticSelector=biasedByExpectedImprovementScore,
                                      startingDesigns=10)
            results = optimizer.runOptimizer()
            finalGuess = results[1][0].design.guess
            print(f"After {results[2].designCount} guess, we guessed {actual} == {finalGuess}")
            assert finalGuess == actual
