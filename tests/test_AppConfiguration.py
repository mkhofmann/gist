from unittest import TestCase

from GIST_App.AppConfiguration import AppParameter, ParamType, AppConfiguration


class TestAppParameter(TestCase):
    def test_to_html(self):
        param = AppParameter("num", "Number", ParamType.Number, 2)
        print(param.toHtml_str())

    def test_toHtml_list(self):
        params = [AppParameter("num", "Number", ParamType.Number, 2), AppParameter("txt", "Text", ParamType.String, "cat"),
                  AppParameter("choice", "Choice", ParamType.choice, "cat", ["cat", "dog"]), AppParameter("bool", "Boolean", ParamType.Boolean, True)]
        appConfig = AppConfiguration(params)
        print(appConfig.toHtml_str())
