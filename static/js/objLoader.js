let objectiveNamesToTableRow = {}; // key the objective names with row ids to ObjectiveRow structures
let objectivesToInstanceCount = {};

function getTable(){
  return document.getElementById("obj_table");
}

function getTableBody(){
  return document.getElementById("obj_table_body")
}


let objParameters = null;
function getObjectiveParamData(){
  if( objParameters === null) {
    objParameters = {};
    let paragraph = document.getElementById("objsToAttributes")
    let text = paragraph.innerText;
    let parsed_data = JSON.parse(text);
    for (let objectiveName in parsed_data) {
      if(parsed_data.hasOwnProperty(objectiveName)) {
        let param_data = parsed_data[objectiveName];
        let namesToParams = {};
        for (let param_name in param_data) {
          if(param_data.hasOwnProperty(param_name)) {
            let param_values = param_data[param_name];
            namesToParams[param_name] = new Parameter(param_name, param_values['type'], param_values['value']);
          }
        }
        objParameters[objectiveName] = new ParameterSet(objectiveName, namesToParams);
      }
    }
  }
}



class Parameter{
  constructor(paramName, type, value) {
    this.paramName = paramName;
    this.type = type;
    this.value = value;
  }

  get copy(){
    return new Parameter(this.paramName, this.type, this.value);
  }

  get inputType(){
    switch (this.type){
      case "bool":
        return "checkbox";
      case "float":
      case "int":
        return "number";
      default:
        return "text";
    }
  }

  getInput(functionName,instanceName){
    return document.getElementById(this.getId(functionName, instanceName));
  }

  update(functionName, instanceName){
    let input = this.getInput(functionName,instanceName);
    if(input.getAttribute("type") === "checkbox"){
      this.value = input.checked;
    }else{
      let number = Number(input.value);
      if(isNaN(number)) {
        this.value = input.value;
      }else{
        this.value = number
      }
    }
    updateAllWeightOptions();
  }

  makeLabel(objectiveName, instanceName){
    let label  = document.createElement("label");
    label.setAttribute("for", this.getId(objectiveName,instanceName));
    label.innerText = this.paramName+": ";
    label.setAttribute("class", "wide_elm");
    return label;
  }

  makeInput(functionName, instanceName){
    let input = document.createElement("input");
    input.setAttribute("class", "wide_elm");
    input.setAttribute("type", this.inputType);
    input.setAttribute("id", this.getId(functionName,instanceName))
    input.setAttribute("data-funcName", functionName);
    input.setAttribute("data-paramName", this.paramName);
    input.setAttribute("data-instanceName", instanceName);
    input.setAttribute("onchange", "updateParameter(this);")
    if( this.type === "bool"){
      if( typeof(this.value) === "boolean"){
        input.checked = this.value;
      } else input.checked = this.value === "True";
    }else {
      input.setAttribute("value", this.value);
      input.setAttribute("size", 5);
    }
    return input;
  }

  getId(functionName, instanceName) {
    return functionName+instanceName + "_" + this.paramName;
  }

  get pythonValue(){
    if(this.type === "bool") {
      if (this.value === true) {
        return "True";
      } else if (this.value === false) {
        return "False";
      } else {
        return this.value;
      }
    }else{
      return this.value;
    }
  }
}

class ParameterSet {
  constructor(functionName, namesToParameters) {
    this.name = functionName;
    this.namesToParameters = namesToParameters;
  }

  get namesToParameter_Copy(){
    let copy = {};
    for(let name in this.namesToParameters){
      if(this.namesToParameters.hasOwnProperty(name)) {
        copy[name] = this.namesToParameters[name].copy;
      }
    }
    return copy;
  }

  makeInputs(parentTag, instanceName) {
    for( let paramName in this.namesToParameters){
      if(this.namesToParameters.hasOwnProperty(paramName)) {
        let parameter = this.namesToParameters[paramName];
        let label = parameter.makeLabel(this.name, instanceName);
        let input = parameter.makeInput(this.name, instanceName);
        parentTag.append(label);
        parentTag.append(document.createElement("br"));
        parentTag.append(input);
        parentTag.append(document.createElement("br"));
      }
    }
  }

  data(){
    let paramNamesToValues = {};
    let paramName;
    for( paramName in this.namesToParameters){
      if(this.namesToParameters.hasOwnProperty(paramName)) {
        let param = this.namesToParameters[paramName];
        paramNamesToValues[paramName] = param.pythonValue;
      }
    }
    return paramNamesToValues;
  }

}

class ObjectiveRow{
  constructor(objName) {
    this.objName = objName;
    this.instance_count = 0;
    if(objectivesToInstanceCount.hasOwnProperty(this.objName)){
      objectivesToInstanceCount[this.objName] +=1;
      this.instance_count = objectivesToInstanceCount[this.objName];
    }else{
      objectivesToInstanceCount[this.objName] = this.instance_count;
    }
    this.numberID = this.objName + "_NumInput"
    this.removeID = "remove_"+this.objName;
    getObjectiveParamData(); // load default param data
    this.parameterInputs = new ParameterSet(this.objName, objParameters[objName].namesToParameter_Copy);
    this.row = null;
  }

  get objWeight(){
    return Number(this.weightSetter.value);
  }

  get data(){
    return {"objWeight": this.objWeight, "paramValues": this.parameterInputs.data(), "objName": this.objName}
  }

  makeRow(){
    let tableBody = getTableBody();
    this.row = tableBody.insertRow();

    //add cells to table
    let weight_cell = this.row.insertCell();
    this._makeWeightCell(weight_cell);
    let obj_cell = this.row.insertCell();
    this._makeInfoCell(obj_cell);
    let param_cell = this.row.insertCell();
    this._makeParamCell(param_cell)
    let remove_cell = this.row.insertCell();
    this._makeRemoveCell(remove_cell);
  }

  _makeWeightCell(weight_cell){
    // Make a Weight cell of this style
    // <td className="info_weight_cell">
    //   <label for="information_weight_0" hidden> Value of This Information</label>
    //   <select name="information_weight" id="information_weight_0" form="settings">
    //     <option value="1">1</option>
    //   </select>
    // </td>
    let weight_label = document.createElement("label");
    weight_label.setAttribute("for", this.numberID);
    // weight_label.setAttribute("class", "hiddenLabels");
    weight_label.innerHTML = "Weight: ";
    weight_cell.append(weight_label);
    let weight_num = document.createElement("input");
    weight_num.setAttribute("type", "number");
    weight_num.setAttribute("id", this.numberID);
    weight_num.setAttribute("value", "1.0");
    weight_num.setAttribute("size", 3);
    weight_num.setAttribute("min", "0");
    weight_num.setAttribute("onchange", "updateAllWeightOptions()")
    weight_cell.append(weight_num);
  }

  get instanceName(){
    return this.objName + "_"+this.instance_count;
  }

  _makeInfoCell(info_cell){
    //Build the info cell
    let cell_text = document.createTextNode(this.instanceName);
    info_cell.append(cell_text);
  }

  _makeRemoveCell(remove_cell){
    //Build remove Button
    // <td className="button_cells remove_button_cell">
    //   <label for="remove_information_0" class="hiddenLabels">Remove Information</label>
    //   <input type="button" id="remove_information_0" value="Remove">
    // </td>
    remove_cell.setAttribute("class", "button_cells remove_button_cell");
    let remove_label = document.createElement("label");
    remove_label.setAttribute("for", this.removeID);
    remove_label.setAttribute("class", "hiddenLabels");
    remove_label.innerHTML = "Remove "+this.objName;
    let remove_button = document.createElement("input");
    remove_button.setAttribute("id", this.removeID);
    remove_button.setAttribute("class", " button alert")
    remove_button.setAttribute("type", "button");
    remove_button.setAttribute("value", "Remove");
    remove_button.setAttribute("data-objectiveName", this.instanceName);
    let removeFunctionCall = "removeObjectiveRow(this)";
    remove_button.setAttribute("onclick", removeFunctionCall);

    remove_cell.append(remove_label);
    remove_cell.append(remove_button);
  }

  removeRow(){
    let table = getTable();
    delete objectiveNamesToTableRow[this.instanceName];
    table.deleteRow(this.row.rowIndex);
    this.row = null;

    updateAllWeightOptions();
    updateObjectiveOptions();

    document.getElementById("add_obj_trigger").disabled = false;
  }

  get weightSetter(){
    return document.getElementById(this.numberID);
  }

  _makeParamCell(obj_cell) {
    let objectiveParams = objParameters[this.objName];
    let obj_div = document.createElement("div");
    obj_cell.append(obj_div)
    objectiveParams.makeInputs(obj_div, "_"+this.instance_count);
  }
}


function updateAllWeightOptions(){
  dataToJson();
  document.getElementById("settings_submission").disabled = Object.keys(objectiveNamesToTableRow).length <= 0;
}



function dataToJson(){
  let dataToWeights = {};
  for(let key in objectiveNamesToTableRow){
    dataToWeights[key] = objectiveNamesToTableRow[key].data;
  }
  let table_text = JSON.stringify(dataToWeights);
  document.getElementById("objWeightMap_text").setAttribute("value", table_text);
  dataToWeights = {};
  for(let key in tacticNamesToTacticData){
    dataToWeights[key] = tacticNamesToTacticData[key].data;
  }
  table_text = JSON.stringify(dataToWeights);
  document.getElementById("tacticWeight_text").setAttribute("value", table_text);
}

// noinspection JSUnusedGlobalSymbols
function addOption(option_val, selection) {
  let option = document.createElement("option");
  option.setAttribute("value", String(option_val));
  option.innerHTML = String(option_val);
  selection.append(option);
}

function addObjective(){
  let addObjSelection = document.getElementById("add_obj");
  let obj_name = addObjSelection.value;
  let objectiveRow = new ObjectiveRow(obj_name);
  objectiveRow.makeRow();//make this specific row
  objectiveNamesToTableRow[objectiveRow.instanceName] = objectiveRow;//update globals
  updateAllWeightOptions();
  if(addObjSelection.length === 0){ //if no choices remain disable this button
    document.getElementById("add_obj_trigger").disabled = true;
  }
  updateObjectiveOptions();

}

// noinspection JSUnusedGlobalSymbols
function removeObjectiveRow(removeButton){
  let objName = removeButton.getAttribute("data-objectiveName");
  for( let tactic_name in tacticNamesToTacticData){
    let tactic = tacticNamesToTacticData[tactic_name]
    tactic.removeObjective(objName);
  }
  let row = objectiveNamesToTableRow[objName];
  row.removeRow();
}

// noinspection JSUnusedGlobalSymbols
function updateParameter(inputElement){
  let functionName = inputElement.getAttribute("data-funcName");
  let paramName = inputElement.getAttribute("data-paramName");
  let instanceName = inputElement.getAttribute("data-instanceName");
  let fullName = functionName+instanceName;
  if( objectiveNamesToTableRow.hasOwnProperty(fullName)){
    objectiveNamesToTableRow[fullName].parameterInputs.namesToParameters[paramName].update(functionName,instanceName);
  }else{
    tacticNamesToTacticData[fullName].parameterInputs.namesToParameters[paramName].update(functionName, instanceName);
    tacticParameters[functionName].namesToParameters[paramName].update(functionName);
  }
}

function submitConfig(){
  document.getElementById("settings").submit()
  // document.getElementById("configForm").submit()
}