shown_results = {};

class Result{
    constructor(rowNum, result_score, result_data) {
        this.rowNum = rowNum;
        this.objectiveData = result_data["objectives"];
        this.shortPrint = result_data["short_print"];
        this.longPrint = JSON.stringify(result_data["long_print"]);
        this.result_score = result_score;
    }

    addFrame() {
        let accordion = document.getElementById("results_accordion");

        let frame = document.getElementById(this.frameId);
        if (frame === null) {
            frame = document.createElement("div");
            frame.setAttribute("id", this.frameId);
        }else{
            frame.innerHTML = "";
        }
        if(this.rowNum === 1){
            frame.setAttribute("class", "frame active");
        }else {
            frame.setAttribute("class", "frame");
        }
        accordion.append(frame);

        this.makeHeader(frame);

        let contentDiv = document.createElement("div");
        contentDiv.setAttribute("class", "content");
        if(this.rowNum === 1){
            contentDiv.setAttribute("style", "display: block;");
        }else{
            contentDiv.setAttribute("style", "display: none;");
        }
        frame.append(contentDiv);

        let resultDiv = document.createElement("div");
        resultDiv.setAttribute("id", "results"+this.rowNum+"_div");
        resultDiv.setAttribute("class", "results_table");
        contentDiv.append(resultDiv);

        let table = this.buildTable();
        resultDiv.append(table);

        let submitDiv = document.createElement("div");
        submitDiv.setAttribute("class", "submit_result");
        contentDiv.append(submitDiv);
        let resultForm = document.createElement("form");
        this.resultFormID = "result_form"+this.rowNum;
        resultForm.setAttribute("id", this.resultFormID);
        resultForm.setAttribute("method", "post");
        submitDiv.append(resultForm)
        let resultShortText  = document.createElement("input");
        resultShortText.setAttribute("type", "text");
        this.shortTextId = "result_"+this.rowNum+"_short_text";
        resultShortText.setAttribute("id", this.shortTextId);
        resultShortText.setAttribute("name", "result_shortPrint");
        resultShortText.setAttribute("form", this.resultFormID);
        // resultShortText.hidden = true;
        resultForm.append(resultShortText);
        let resultLongText  = document.createElement("input");
        resultLongText.setAttribute("type", "text");
        this.longTextId = "result_"+this.rowNum+"_long_text";
        resultLongText.setAttribute("id", this.longTextId);
        resultLongText.setAttribute("name", "result_longPrint");
        resultLongText.setAttribute("form", this.resultFormID);
        // resultLongText.hidden = true;
        resultForm.append(resultLongText);

        let submitLabel = document.createElement("label");
        let submitId = "submit_result_"+this.rowNum;
        submitLabel.setAttribute("for", submitId);
        submitDiv.append(submitLabel);
        let submitInput = document.createElement("input");
        submitInput.setAttribute("id", submitId);
        submitInput.setAttribute("class", "button success");
        submitInput.setAttribute("type", "button");
        submitInput.setAttribute("value", "Accept Result "+this.rowNum);
        submitInput.setAttribute("form", this.resultFormID);
        submitInput.setAttribute("onclick", "shown_results["+this.rowNum+"].updateResult()")
        submitDiv.append(submitInput);
    }

    // noinspection JSUnusedGlobalSymbols
    updateResult(){
        // let result_text = document.getElementById("result_longPrint");
        // result_text.setAttribute("value", this.longPrint);
        //
        // result_text = document.getElementById("result_shortPrint");
        // result_text.setAttribute("value", this.shortPrint);
        //
        // let outputButton = document.getElementById("output_result");
        // outputButton.hidden = false;
        // outputButton.disabled = false;

        let shortText = document.getElementById(this.shortTextId);
        shortText.setAttribute("value", this.shortPrint);

        let longText = document.getElementById(this.longTextId);
        longText.setAttribute("value", this.longPrint);

        document.getElementById(this.resultFormID).submit();
    }

    makeHeader(frame) {
        let heading_div = document.createElement("div");
        heading_div.setAttribute("class", "heading");
        frame.append(heading_div);

        let heading = document.createElement("h2");
        let headerStr = "Result " + this.rowNum + " (Total Score: " + this.result_score + ")";
        if(this.shortPrint !== "None"){
            headerStr += ": "+this.shortPrint;
        }
        heading.innerText = headerStr;
        heading_div.append(heading);
    }

    get frameId() {
        return "result_" + this.result_score;
    }

    buildTable() {
        let table = document.createElement("table");
        table.setAttribute("id", "results_" + this.rowNum + "_table");
        table.setAttribute("class", "table striped row-hover");
        let thead = table.createTHead();
        let headerRow = thead.insertRow();
        headerRow.insertCell().innerText = "Importance";
        headerRow.insertCell().innerText = "Objective";
        headerRow.insertCell().innerText = "0 <= Score <= 1";
        headerRow.insertCell().innerText = "Weighted Score (I*S)";
        let body = table.createTBody();
        for (let objective_name in this.objectiveData) {
            if(this.objectiveData.hasOwnProperty(objective_name)) {
                let data = this.objectiveData[objective_name];
                let row = body.insertRow();
                let obj_weight = Number(data["obj_weight"]);
                let score = Number(data["base_score"]);
                row.insertCell().innerText = String(obj_weight);
                row.insertCell().innerText = objective_name;
                row.insertCell().innerText = String(score);
                row.insertCell().innerText = String(obj_weight * score);
            }
        }
        return table;
    }
}

let isFirstLoad = true;

function loadResults(){
    let paragraph = document.getElementById("resultsFromPython");
    let text = paragraph.innerText;
    if(isFirstLoad) {
        let results = JSON.parse(text);
        let i = 1;
        for (let result_score_str in results) {
            if(results.hasOwnProperty(result_score_str)){
                let result_score = Number(result_score_str);
                let result_data = results[result_score_str];
                let result = new Result(i, result_score, result_data);
                shown_results[i] = result;
                result.addFrame();
                i++;
            }
        }
        let loadButton = document.getElementById("load_results");
        loadButton.value = "Update Results";
        isFirstLoad = false;
    }else{
        document.getElementById("update_form").submit();
    }


}

