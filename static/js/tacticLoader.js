let tacticNamesToTacticData = {};
let tacticParameters = null;
class TacticData{
    constructor(tactic_name, frame_num) {
        this.tactic_name = tactic_name;
        this.frame_num = frame_num;
        this.objectivesToWeights = {}; // objectiveNames to weights
        this.objectivesToRows = {};
        this.objective_table_body = null;
        this.objective_selector = null;
        getTacticParamData(); // load default parameter data
        this.parameterInputs = new ParameterSet(this.tactic_name, tacticParameters[tactic_name].namesToParameter_Copy)
        this.makeTacticFrame();
    }

    get data(){
        return  {"tacticName": this.tactic_name, "paramValues": this.parameterInputs.data(), "objectives": this.objectivesToWeights};
    }

    get objectivesID(){
        return this.tactic_num+"_objectives";
    }

    makeTacticFrame(){
        let accordion = document.getElementById("tactic_accordion");
        let frame_div = document.createElement("div");
        accordion.append(frame_div);
        if( this.frame_num === 1){
            frame_div.setAttribute("class", "frame active");
        }else{
            frame_div.setAttribute("class", "frame");
        }
        frame_div.setAttribute("id", this.tactic_num);
        this.makeHeader(frame_div);
        let content_div = document.createElement("div");
        frame_div.append(content_div);
        content_div.setAttribute("class", "content");
        if(this.frame_num === 1){
            content_div.setAttribute("style", "display: block;");
        }else{
            content_div.setAttribute("style", "display: none;");
        }
        this.addParameters(content_div);
        this.makeObjectiveSelector(content_div);
        this.makeObjectiveTable(content_div);
    }


    makeObjectiveSelector(content_div) {
        let label = document.createElement("label");
        content_div.append(label);
        label.setAttribute("for", this.objectivesID);
        label.innerText = "Available Objectives";
        label.hidden = true;
        this.objective_selector = document.createElement("select");
        content_div.append(this.objective_selector);
        this.objective_selector.setAttribute("name", this.objectivesID);
        this.objective_selector.setAttribute("id", this.objectivesID);
        this.objective_selector.setAttribute("class", "wide_elm");
        this.updateObjectiveSelection();
        label = document.createElement("label");
        content_div.append(label);
        let addId = this.tactic_num + "_addObj";
        label.setAttribute("for", addId);
        label.setAttribute("class", "hiddenLabels");
        let trigger = document.createElement("input");
        content_div.append(trigger);
        trigger.setAttribute("id", addId);
        trigger.setAttribute("type", "button");
        trigger.setAttribute("class", "button wide_elm");
        trigger.setAttribute("value", "Add Objective");
        trigger.setAttribute("data-tacticName", this.tactic_num);
        trigger.setAttribute("onclick", "addObjectiveTacticWeight(this)");
        return label;
    }

    updateObjectiveSelection() {
        let wasDisabled = {};
        for(let option in this.objective_selector.options){
            if(option === "length"){
                break;
            }
            if(this.objective_selector.options.hasOwnProperty(option)) {
                option = this.objective_selector.options[option];
                wasDisabled[option.id] = option.disabled;
            }
        }
        this.objective_selector.innerHTML = '';
        for (let objective_name in objectiveNamesToTableRow) {
            let option = document.createElement("option");
            this.objective_selector.append(option);
            option.setAttribute("value", objective_name);
            let optionId = this.tactic_num+"_"+objective_name + "_option";
            option.setAttribute("id", optionId);
            if(wasDisabled.hasOwnProperty(optionId)) {
                option.disabled = wasDisabled[optionId];
            }
            option.innerText = objective_name;
        }
        this.objective_selector.selectedIndex = -1;
    }

    makeObjectiveTable(content_div) {
        let table_div = document.createElement("div");
        content_div.append(table_div);
        table_div.setAttribute("id", this.tactic_num + "_div");
        table_div.setAttribute("class", "tactic_table");
        let table = document.createElement("table");
        table_div.append(table);
        table_div.setAttribute("id", this.tactic_num + "_table");
        table_div.setAttribute("class", "table striped row-hover");
        let header = table.createTHead();
        let row = header.insertRow();
        let cell = row.insertCell();
        cell.innerText = "Tactic's Importance to Objective";
        cell = row.insertCell();
        cell.innerText = "Objective";
        row.insertCell();
        this.objective_table_body = table.createTBody();
    }

    makeHeader(frame_div) {
        let heading = document.createElement("div");
        frame_div.append(heading);
        heading.setAttribute("class", "heading");
        let h2 = document.createElement("h2");
        heading.append(h2);
        h2.innerText = this.tactic_name + " " + this.frame_num;
    }

    get tactic_num(){
        return this.tactic_name + "_" +this.frame_num;
    }

    updateObjective(objective_name, weight){
        this.objectivesToWeights[objective_name] = weight;
    }

    addObjectiveRow(){
        let selection = document.getElementById(this.objectivesID);
        let objective_name = selection.value;
        let option = document.getElementById(this.tactic_num+"_"+objective_name+"_option");
        option.disabled = true;
        option.selected = false;
        let row = this.objective_table_body.insertRow();
        this.objectivesToRows[objective_name] = row;
        let weight_cell = row.insertCell();
        let label = document.createElement("label");
        weight_cell.append(label);
        let input_id = this.tactic_num+"_"+objective_name+"_weight";
        label.setAttribute("for", input_id);
        label.setAttribute("class", "hiddenLabels");
        let input = document.createElement("input");
        weight_cell.append(input);
        input.setAttribute("type", "number");
        input.setAttribute("value", "1.0");
        let obj_cell = row.insertCell();
        obj_cell.innerText = objective_name;
        let remove_cell= row.insertCell();
        label = document.createElement("label");
        remove_cell.append(label);
        let removeId = this.tactic_num+"_"+objective_name+"_remove";
        label.setAttribute("for", removeId);
        label.setAttribute("class", "hiddenLabels");
        label.innerHTML = "Remove";
        let remove_button = document.createElement("input");
        remove_cell.append(remove_button);
        remove_button.setAttribute("id", removeId);
        remove_button.setAttribute("class", "button alert")
        remove_button.setAttribute("type", "button");
        remove_button.setAttribute("value", "Remove");
        remove_button.setAttribute("data-objectiveName", objective_name);
        remove_button.setAttribute("data-tacticName", this.tactic_num);
        let removeFunctionCall = "removeObjectiveFromTactic(this)";
        remove_button.setAttribute("onclick", removeFunctionCall);

        this.updateObjective(objective_name, 1.0);
    }
    removeObjective(objective_name){
        if(this.objectivesToRows.hasOwnProperty(objective_name)) {
            let obj_row = this.objectivesToRows[objective_name];
            this.objective_table_body.deleteRow(obj_row.rowIndex - 1);
            delete this.objectivesToRows[objective_name];
            delete this.objectivesToWeights[objective_name];
            let option = document.getElementById(this.objectiveOptionID(objective_name));
            option.disabled = false;
        }
    }


    objectiveOptionID(objective_name) {
        return this.tactic_num + "_" + objective_name + "_option";
    }

    addParameters(heading) {
        this.parameterInputs.makeInputs(heading, "_" + this.frame_num);
    }
}

function addTactic(){
    let frame_count = Object.keys(tacticNamesToTacticData).length;
    let frame_num = frame_count +1;
    let tactic_name = document.getElementById("add_tactic").value;
    let tacticData = new TacticData(tactic_name, frame_num);
    tacticNamesToTacticData[tacticData.tactic_num] = tacticData;
    updateAllWeightOptions();

}

// noinspection JSUnusedGlobalSymbols
function addObjectiveTacticWeight(addObjectiveButton){
    let tacticName = addObjectiveButton.getAttribute("data-tacticName");
    let tactic = tacticNamesToTacticData[tacticName];
    tactic.addObjectiveRow();
    updateAllWeightOptions();
}

function updateObjectiveOptions(){
    for(let tactic_name in tacticNamesToTacticData){
        let tactic_data = tacticNamesToTacticData[tactic_name];
        tactic_data.updateObjectiveSelection();
    }
}

// noinspection JSUnusedGlobalSymbols
function removeObjectiveFromTactic(removeButton){
    let objName = removeButton.getAttribute("data-objectiveName");
    let tacticName = removeButton.getAttribute("data-tacticName");
    let tactic = tacticNamesToTacticData[tacticName];
    tactic.removeObjective(objName);
    updateAllWeightOptions();
}

function getTacticParamData(){
    if( tacticParameters === null) {
        tacticParameters = {};
        let paragraph = document.getElementById("tacticsToAttributes")
        let text = paragraph.innerText;
        let parsed_data = JSON.parse(text);
        for (let tacticName in parsed_data) {
            if(parsed_data.hasOwnProperty(tacticName)) {
                let param_data = parsed_data[tacticName];
                let namesToParams = {};
                for (let param_name in param_data) {
                    if(param_data.hasOwnProperty(param_name)) {
                        let param_values = param_data[param_name];
                        namesToParams[param_name] = new Parameter(param_name, param_values['type'], param_values['value']);
                    }
                }
                tacticParameters[tacticName] = new ParameterSet(tacticName, namesToParams);
            }
        }
    }
}

function displayTacticChoices(selector){
    let selection = selector.value;
    document.getElementById("tactic_map").hidden = selection === "learn" || selection === "default";
}
